#ifndef OPERATORTOKEN_HPP_INCLUDED
#define OPERATORTOKEN_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <string> //std::string

namespace ec {
class OperatorToken {
public:
	enum class Type {
		Function,
		BinaryOperator,
		UnaryOperator,
		OpeningBracket,
		ClosingBracket,
		Error
	};

	OperatorToken ( Type type, std::string lexeme );

	Type getType() const;
	const std::string& getLexeme() const;

private:
	Type m_Type;
	std::string m_Lexeme;
};
}

#endif // OPERATORTOKEN_HPP_INCLUDED
