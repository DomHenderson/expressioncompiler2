#ifndef COMPILER_HPP_INCLUDED
#define COMPILER_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional>    //std::function
#include <unordered_map> //std::unordered_map
#include <set>           //std::set
#include <string>        //std::string

//INTERNAL INCLUDES
#include "lexer.hpp"        //ec::Lexer
#include "parser.hpp"       //ec::Parser
#include "preprocessor.hpp" //ec::Preprocessor
#include "shuntingYard.hpp" //ec::ShuntingYard

namespace ec {

class Compiler {
public:
	Compiler( std::function<void(std::string)> logError );

	std::function<double(std::unordered_map<std::string,double>&)> compile ( std::string expression );
	std::string differentiate ( std::string expression, std::string variable );
	std::set<std::string> getVariableNames ( std::string expression );

	void addConstant ( std::string name, double value );
	void removeConstant ( std::string name );
	void addFunction ( std::string name, std::string variable, std::string expression );
	void removeFunction ( std::string name );

	enum class DRG {
		Degrees,
		Radians,
		Gradians
	};
	void setTrigMode ( DRG mode );

private:
	Preprocessor m_Preprocessor;
	Lexer m_Lexer;
	ShuntingYard m_ShuntingYard;
	Parser m_Parser;

	std::function<void(std::string)> m_LogError;
};

}

#endif // COMPILER_HPP_INCLUDED
