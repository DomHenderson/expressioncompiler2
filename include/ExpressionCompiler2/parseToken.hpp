#ifndef PARSETOKEN_HPP_INCLUDED
#define PARSETOKEN_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <memory> //std::unique_ptr
#include <queue>  //std::queue
#include <stack>  //std::stack
#include <string> //std::string

//INTERNAL INCLUDES
#include "shuntingToken.hpp" //ec::ShuntingToken

namespace ec {

//FORWARD DECLARATIONS
class ParseNode;
class Parser;

//Abstract base class for all possible types of parsing token. This can never be instantiated.
class ParseToken {
public:
	virtual std::unique_ptr<ParseNode> getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& tokens ) =0;
	virtual void print () const =0;
};

//A parse token representing a variable. The resulting parse node will have no set value, but will be assigned one when the function is called.
class ParameterParseToken: public ParseToken {
public:
	ParameterParseToken ( std::string name );
	virtual std::unique_ptr<ParseNode> getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& tokens ) override;
	virtual void print() const override;
private:
	std::string m_Name;
};

//A parse token representing a constant. The resulting parse node will contain a value which will never change.
class ConstantParseToken: public ParseToken {
public:
	ConstantParseToken ( double value );
	virtual std::unique_ptr<ParseNode> getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& tokens ) override;
	virtual void print() const override;
private:
	double m_Value;
};

//A parse token representing any of the predefined functions. The resulting parse node will hold pointers to its operands and will
//be able to perform various recursive functions such as evaluation, differentiation etc.
class OperationParseToken: public ParseToken {
public:
	enum class Type {
		Addition,
		Subtraction,
		Multiplication,
		Division,
		Power,
		Negation,
		Acosech,
		Acosec,
		Cosech,
		Acosh,
		Acoth,
		Asech,
		Asinh,
		Atanh,
		Cosec,
		Acos,
		Acot,
		Asec,
		Asin,
		Atan,
		Cosh,
		Coth,
		Log2,
		Sech,
		Sinh,
		Tanh,
		Abs,
		Cos,
		Cot,
		Exp,
		Log,
		Sec,
		Sin,
		Tan,
		Ln
	};

	OperationParseToken ( Type type );
	virtual std::unique_ptr<ParseNode> getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& tokens ) override;
	virtual void print() const override;
private:
	Type m_Type;
};

//A parse token representing a user defined function. It uses the parser to generate a subtree rather than a single node.
class CustomFunctionParseToken: public ParseToken {
public:
	CustomFunctionParseToken ( std::string name, Parser& parser );
	virtual std::unique_ptr<ParseNode> getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& tokens ) override;
	virtual void print() const override;
private:
	Parser& m_Parser;

	std::string m_Name;
};

//This represents the independent variable in a user defined function.
//e.g. in f:x->ax, a would be a normal parameter and x would be a custom function input.
//This allows the parser to only replace the relevant tokens when inserting the passed expression into the function.
class CustomFunctionInputParseToken: public ParseToken {
public:
	CustomFunctionInputParseToken ( ParseNode::Ptr node );
	virtual std::unique_ptr<ParseNode> getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& tokens ) override;
	virtual void print() const override;
private:
	ParseNode::Ptr m_Node;
};

}

#endif // PARSETOKEN_HPP_INCLUDED
