#ifndef PARSENODE_HPP_INCLUDED
#define PARSENODE_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional>    //std::function
#include <memory>        //std::unique_ptr
#include <queue>         //std::queue
#include <string>        //std::string
#include <unordered_map> //std::unordered_map

namespace ec {

class ParseToken;

class ParseNode {
public:
	typedef std::unique_ptr<ParseNode> Ptr;

	virtual Ptr getCopy () =0;
	virtual Ptr getDifferential ( std::string variable ) =0;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() =0;
	virtual unsigned getPriority() const =0;
	virtual bool isConstant() =0;
	virtual bool isValid() =0;
	virtual std::string getName() const =0;
	virtual Ptr simplify() =0;

	enum class DRG {
		Degrees,
		Radians,
		Gradians
	};
	static void setDRGMode ( DRG mode );

protected:
	static double DRGConversion;
};

class ParameterParseNode: public ParseNode {
public:
	typedef std::unique_ptr<ParameterParseNode> Ptr;

	ParameterParseNode ( std::string name );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 4; }
	virtual bool isConstant() override { return false; }
	virtual bool isValid() override { return true; }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	std::string m_Name;
};

class ConstantParseNode: public ParseNode {
public:
	typedef std::unique_ptr<ConstantParseNode> Ptr;

	ConstantParseNode ( double value );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 4; }
	virtual bool isConstant() override { return true; }
	virtual bool isValid() override { return true; }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	double m_Value;
};

class AdditionParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AdditionParseNode> Ptr;

	AdditionParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AdditionParseNode ( ParseNode::Ptr left, ParseNode::Ptr right );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 0; }
	virtual bool isConstant() override { return m_Left->isConstant() && m_Right->isConstant(); }
	virtual bool isValid() override { return m_Left && m_Right && m_Left->isValid() && m_Right->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Left;
	ParseNode::Ptr m_Right;
};

class SubtractionParseNode: public ParseNode {
public:
	typedef std::unique_ptr<SubtractionParseNode> Ptr;

	SubtractionParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	SubtractionParseNode ( ParseNode::Ptr left, ParseNode::Ptr right );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 0; }
	virtual bool isConstant() override { return m_Left->isConstant() && m_Right->isConstant(); }
	virtual bool isValid() override { return m_Left && m_Right && m_Left->isValid() && m_Right->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Left;
	ParseNode::Ptr m_Right;
};

class MultiplicationParseNode: public ParseNode {
public:
	typedef std::unique_ptr<MultiplicationParseNode> Ptr;

	MultiplicationParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	MultiplicationParseNode ( ParseNode::Ptr left, ParseNode::Ptr right );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 1; }
	virtual bool isConstant() override { return m_Left->isConstant() && m_Right->isConstant(); }
	virtual bool isValid() override { return m_Left && m_Right && m_Left->isValid() && m_Right->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Left;
	ParseNode::Ptr m_Right;
};

class DivisionParseNode: public ParseNode {
public:
	typedef std::unique_ptr<DivisionParseNode> Ptr;

	DivisionParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	DivisionParseNode ( ParseNode::Ptr left, ParseNode::Ptr right );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 1; }
	virtual bool isConstant() override { return m_Left->isConstant() && m_Right->isConstant(); }
	virtual bool isValid() override { return m_Left && m_Right && m_Left->isValid() && m_Right->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Left;
	ParseNode::Ptr m_Right;
};

class PowerParseNode: public ParseNode {
public:
	typedef std::unique_ptr<PowerParseNode> Ptr;

	PowerParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	PowerParseNode ( ParseNode::Ptr left, ParseNode::Ptr right );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 2; }
	virtual bool isConstant() override { return m_Left->isConstant() && m_Right->isConstant(); }
	virtual bool isValid() override { return m_Left && m_Right && m_Left->isValid() && m_Right->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Left;
	ParseNode::Ptr m_Right;
};

class NegationParseNode: public ParseNode {
public:
	typedef std::unique_ptr<NegationParseNode> Ptr;

	NegationParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	NegationParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 1; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AcosechParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AcosechParseNode> Ptr;

	AcosechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AcosechParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AcosecParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AcosecParseNode> Ptr;

	AcosecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AcosecParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class CosechParseNode: public ParseNode {
public:
	typedef std::unique_ptr<CosechParseNode> Ptr;

	CosechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	CosechParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AcoshParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AcoshParseNode> Ptr;

	AcoshParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AcoshParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AcothParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AcothParseNode> Ptr;

	AcothParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AcothParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AsechParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AsechParseNode> Ptr;

	AsechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AsechParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AsinhParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AsinhParseNode> Ptr;

	AsinhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AsinhParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AtanhParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AtanhParseNode> Ptr;

	AtanhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AtanhParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class CosecParseNode: public ParseNode {
public:
	typedef std::unique_ptr<CosecParseNode> Ptr;

	CosecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	CosecParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AcosParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AcosParseNode> Ptr;

	AcosParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AcosParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AcotParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AcotParseNode> Ptr;

	AcotParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AcotParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AsecParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AsecParseNode> Ptr;

	AsecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AsecParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AsinParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AsinParseNode> Ptr;

	AsinParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AsinParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AtanParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AtanParseNode> Ptr;

	AtanParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AtanParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class CoshParseNode: public ParseNode {
public:
	typedef std::unique_ptr<CoshParseNode> Ptr;

	CoshParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	CoshParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class CothParseNode: public ParseNode {
public:
	typedef std::unique_ptr<CothParseNode> Ptr;

	CothParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	CothParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class Log2ParseNode: public ParseNode {
public:
	typedef std::unique_ptr<Log2ParseNode> Ptr;

	Log2ParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	Log2ParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class SechParseNode: public ParseNode {
public:
	typedef std::unique_ptr<SechParseNode> Ptr;

	SechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	SechParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class SinhParseNode: public ParseNode {
public:
	typedef std::unique_ptr<SinhParseNode> Ptr;

	SinhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	SinhParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class TanhParseNode: public ParseNode {
public:
	typedef std::unique_ptr<TanhParseNode> Ptr;

	TanhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	TanhParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class AbsParseNode: public ParseNode {
public:
	typedef std::unique_ptr<AbsParseNode> Ptr;

	AbsParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	AbsParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class CosParseNode: public ParseNode {
public:
	typedef std::unique_ptr<CosParseNode> Ptr;

	CosParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	CosParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class CotParseNode: public ParseNode {
public:
	typedef std::unique_ptr<CotParseNode> Ptr;

	CotParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	CotParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class ExpParseNode: public ParseNode {
public:
	typedef std::unique_ptr<ExpParseNode> Ptr;

	ExpParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	ExpParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class LogParseNode: public ParseNode {
public:
	typedef std::unique_ptr<LogParseNode> Ptr;

	LogParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	LogParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class SecParseNode: public ParseNode {
public:
	typedef std::unique_ptr<SecParseNode> Ptr;

	SecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	SecParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class SinParseNode: public ParseNode {
public:
	typedef std::unique_ptr<SinParseNode> Ptr;

	SinParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	SinParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class TanParseNode: public ParseNode {
public:
	typedef std::unique_ptr<TanParseNode> Ptr;

	TanParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	TanParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

class LnParseNode: public ParseNode {
public:
	typedef std::unique_ptr<LnParseNode> Ptr;

	LnParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens );
	LnParseNode ( ParseNode::Ptr op );
	virtual ParseNode::Ptr getCopy() override;
	virtual ParseNode::Ptr getDifferential ( std::string variable ) override;
	virtual std::function<double(std::unordered_map<std::string,double>&)> getFunction() override;
	virtual unsigned getPriority() const { return 3; }
	virtual bool isConstant() override { return m_Operand->isConstant(); }
	virtual bool isValid() override { return m_Operand && m_Operand->isValid(); }
	virtual std::string getName() const override;
	virtual ParseNode::Ptr simplify() override;
private:
	ParseNode::Ptr m_Operand;
};

}

#endif // PARSENODE_HPP_INCLUDED
