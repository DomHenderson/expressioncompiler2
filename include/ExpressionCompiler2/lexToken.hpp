#ifndef LEXTOKEN_HPP_INCLUDED
#define LEXTOKEN_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <string> //std::string

namespace ec {

class LexToken {
public:
	enum class Type {
		Function,
		BinaryOperator,
		UnaryOperator,
		Number,
		Variable,
		Bracket,
		Separator
	};

	LexToken ( const std::string& lexeme, Type type );

	void print ();

	Type getType() const;

	const std::string& getLexeme() const;

private:
	std::string m_Lexeme;
	Type m_Type;
};
}

#endif // LEXTOKEN_HPP_INCLUDED
