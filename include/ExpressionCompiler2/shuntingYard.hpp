#ifndef SHUNTINGYARD_HPP_INCLUDED
#define SHUNTINGYARD_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <queue> //std::queue
#include <stack> //std::stack

//INTERNAL INCLUDES
#include "operatorToken.hpp" //ec::OperatorToken

namespace ec {

//FORWARD DECLARATIONS
class LexToken;
class ShuntingToken;

class ShuntingYard {
public:
	ShuntingYard();
	bool getStatus() const;
	std::stack<ShuntingToken> operator()( std::queue<LexToken> tokens );
private:

	class OperatorStack {
	public:
		OperatorStack ( std::stack<ShuntingToken>& output );
		bool getStatus() const;
		void push ( LexToken token );
		void popAll ();
	private:
		OperatorToken CreateOperatorToken ( LexToken token );
		void PrepareForPush( OperatorToken token );
		void HandleClosingBracket ();
		void PopToOutput();
		bool ReadyForPush( OperatorToken token );

		std::stack<ShuntingToken>& m_Output;
		std::stack<OperatorToken> m_Stack;

		bool m_Status;
	};

	bool m_Status;

	std::stack<ShuntingToken> m_Output;

	OperatorStack m_OperatorStack;

};
}

#endif // SHUNTINGYARD_HPP_INCLUDED
