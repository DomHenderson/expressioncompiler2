#ifndef PARSER_HPP_INCLUDED
#define PARSER_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <map>           //std::map
#include <memory>        //std::unique_ptr
#include <queue>         //std::queue
#include <stack>         //std::stack
#include <string>        //std::string

//INTERNAL INCLUDES
#include "parseNode.hpp" //ec::ParseNode::Ptr
#include "shuntingToken.hpp" //ec::ShuntingToken

namespace ec {

//FORWARD DECLARATIONS
class ParseToken;

class Parser {
public:
	Parser();
	ParseNode::Ptr operator() ( std::stack<ShuntingToken> tokens );
	ParseNode::Ptr getCustomFunctionTree ( std::string name, std::queue<std::unique_ptr<ParseToken>>& inputTokens );
	void addFunction ( std::string name, std::string variable, std::stack<ShuntingToken> tokens );
	void removeFunction ( std::string name );
	bool getStatus() const;
private:
	bool m_Status;

	std::unique_ptr<ParseToken> BinaryOperatorToToken ( std::string lexeme );
	std::unique_ptr<ParseToken> FunctionToToken ( std::string lexeme );
	std::unique_ptr<ParseToken> NumberToToken ( std::string lexeme );
	std::unique_ptr<ParseToken> UnaryOperatorToToken ( std::string lexeme );
	std::unique_ptr<ParseToken> VariableToToken ( std::string lexeme );

	struct ReferenceFunction {
		std::string variable;
		std::stack<ShuntingToken> tokens;
	};
	//User defined functions that can be used from within other functions
	std::map<std::string, ReferenceFunction> m_ReferenceFunctions;
};

}

#endif // PARSER_HPP_INCLUDED
