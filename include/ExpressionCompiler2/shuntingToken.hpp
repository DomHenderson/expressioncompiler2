#ifndef SHUNTINGTOKEN_HPP_INCLUDED
#define SHUNTINGTOKEN_HPP_INCLUDED

//STANDRAD LIBRARY INCLUDES
#include <string> //std::string

namespace ec {

class ShuntingToken {
public:
	enum class Type { //This is a subset of the values of LexToken::Type
		Function,
		BinaryOperator,
		UnaryOperator,
		Number,
		Variable,
		Error //This stores information about an error in the shunting process
	};

	ShuntingToken ( const std::string& lexeme, Type type );

	void print();

	Type getType() const;

	const std::string& getLexeme() const;
private:
	std::string m_Lexeme;
	Type m_Type;
};

}

#endif // SHUNTINGTOKEN_HPP_INCLUDED
