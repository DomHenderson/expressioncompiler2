#ifndef PREPROCESSOR_HPP_INCLUDED
#define PREPROCESSOR_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <string>  //std::string
#include <map>  //std::map

namespace ec {

class Preprocessor {
public:
	Preprocessor();
	bool getStatus();
	std::string operator()( const std::string& expression);

	void addConstant ( std::string constantName, double value );
	void removeConstant ( std::string constantName );
	void addFunction ( std::string name );
	void removeFunction ( std::string functionName );
private:
	void CheckNotBlank ( const std::string& expression );
	void FindIllegalCharacters( const std::string& expression );
	void RemoveIgnoredCharacters ( std::string& expression );
	void MakeReplacements ( std::string& expression );
	void ReplaceUnaryNegation ( std::string& expression );
	void InsertImpliedMultiplication ( std::string& expression );

	bool m_Status;

	struct KeyComparator { //Should return true if left goes before right and false if right should go before left
	public:
		bool operator () ( std::string left, std::string right ) {
			if ( left.length() > right.length() ) { //All longer strings go before shorter strings
				return true;
			} else if ( left.length() == right.length() ) {
				return left < right; //Strings of equal length must still be sorted.
				                     //This is because map treats two keys as equivalent if !comp(a,b) && !comp(b,a)
			} else {
				return false;
			}
		}
	};

	std::map<std::string,std::string,KeyComparator> m_Replacements;
};

}

#endif // PREPROCESSOR_HPP_INCLUDED
