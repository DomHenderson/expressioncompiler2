#ifndef LEXER_HPP_INCLUDED
#define LEXER_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <array>   //std::array
#include <queue>   //std::queue
#include <regex>   //std::regex
#include <string>  //std::string
#include <utility> //std::pair

//INTERNAL INCLUDES
#include "lexToken.hpp" //ec::LexToken

namespace ec {

class Lexer {
public:
	Lexer();
	bool getStatus() const;
	std::queue<LexToken> operator()( std::string expression );
private:
	bool m_Status;

	std::array<std::pair<std::regex, LexToken::Type>,7> m_LexTokenIdentifiers;
};

}

#endif // LEXER_HPP_INCLUDED
