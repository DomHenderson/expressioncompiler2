#ifndef CLASSIFIER_HPP_INCLUDED
#define CLASSIFIER_HPP_INCLUDED

namespace ec::Classifier {

enum class Type {
	Letter,
	Number,
	Point,
	Operator,
	Bracket,
	Whitespace,
	Bad
};

Type Classify ( char character );

}

#endif // CLASSIFIER_HPP_INCLUDED
