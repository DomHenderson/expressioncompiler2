A library implementing runtime compilation of mathematical expressions into executable functions.
Symbolic differentiation is also provided.
The Compiler class provides an easily usable interface.

The process of compilation is done in much the same way as with language compilers.
First preprocessing, then lexing, parsing and finally optimisation.
Each section is encapsulated into its own class.
This design allows for easy extension into a more extensive language compiler.