//STANDARD LIBRARY INCLUDES
#include <iostream> //std::cout, std::endl
#include <stack>    //std::stack
#include <queue>    //std::queue

//INTERNAL INCLUDES
#include "lexToken.hpp"      //ec::LexToken
#include "shuntingToken.hpp" //ec::ShuntingToken
#include "shuntingYard.hpp"  //ec::ShuntingYard

namespace ec {

ShuntingYard::ShuntingYard () :
	m_Status ( true ),
	m_OperatorStack ( m_Output )
{
}

bool ShuntingYard::getStatus() const
{
	return m_Status;
}

std::stack<ShuntingToken> ShuntingYard::operator()( std::queue<LexToken> tokens )
{
	std::cout<<"Started shunting"<<std::endl;
	m_Status = true;

	//Empty the output queue of the previous expression.
	m_Output = std::stack<ShuntingToken>();

	//Tokens are either pused directly to the output stack, or to the OperatorStack, which handles the actual reordering.
	//This is done in accordance with Djikstra's shunting yard algorithm.
	while ( !tokens.empty() ) {
		std::cout<<"Looping"<<std::endl;

		LexToken& token = tokens.front();

		switch ( token.getType() ) {
		case LexToken::Type::BinaryOperator:
			std::cout<<"Binary operator:	";
			m_OperatorStack.push ( token );
			break;

		case LexToken::Type::Bracket:
			std::cout<<"Bracket:	";
			m_OperatorStack.push ( token );
			break;

		case LexToken::Type::Function:
			std::cout<<"Function:	";
			m_OperatorStack.push ( token );
			break;

		case LexToken::Type::Number:
			std::cout<<"Number"<<std::endl;
			m_Output.push ( ShuntingToken ( token.getLexeme(), ShuntingToken::Type::Number ) );
			break;

		case LexToken::Type::Separator:
			std::cout<<"Separator:	";
			m_OperatorStack.push ( token );
			break;

		case LexToken::Type::UnaryOperator:
			std::cout<<"Unary operator:	";
			m_OperatorStack.push ( token );
			break;

		case LexToken::Type::Variable:
			std::cout<<"Variable"<<std::endl;;
			m_Output.push ( ShuntingToken ( token.getLexeme(), ShuntingToken::Type::Variable ) );
			break;
		}

		if ( m_OperatorStack.getStatus() == false ) {
			std::cout<<"Operator stack status false"<<std::endl;
			m_Status = false;
			m_OperatorStack.popAll();
			return m_Output;
		}

		tokens.pop();
	}

	std::cout<<"Loop ended"<<std::endl;

	m_OperatorStack.popAll();

	std::cout<<"Done"<<std::endl;

	return m_Output;
}

}
