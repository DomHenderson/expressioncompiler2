//STANDARD LIBRARY INCLUDES
#include <functional>    //std::function
#include <queue>         //std::queue
#include <set>           //std::set
#include <stack>         //std::stack
#include <string>        //std::string
#include <unordered_map> //std::unordered_map

//INTERNAL INCLUDES
#include "compiler.hpp"  //ec::Compiler
#include "parseNode.hpp" //ec::ParseNode

namespace ec {

Compiler::Compiler( std::function<void(std::string)> logError ) :
	m_LogError ( logError ) //The compiler must be supplied with some way of delivering its error messages to the user.
{
}

//The main function of the compiler: turn an expression into a function.
//The unordered_map relates all variables to their values.
std::function<double(std::unordered_map<std::string,double>&)> Compiler::compile ( std::string expression )
{
	//The first step is to preprocess the input.
	//This handles very specific functions so that lexing can be done very generically.
	//Basically, it prepares the string for lexing by making its meaning completely explicit.

	std::string preprocessed = m_Preprocessor ( expression );

	//The compiler must check for errors at every stage.
	//This stops them getting propagated too far and possibly causing a crash. It also helps in diagnosis of errors.
	//The getStatus functions are used for this. They only return false if something went wrong during processing.
	if ( !m_Preprocessor.getStatus() ) {
		m_LogError ( "Preprocessing failed" );
		m_LogError ( "Preprocessed string:" );
		m_LogError ( preprocessed );
		//By returning now, the function is stopped and the erroneous input is not passed any further.
		//This minimises the chance of it causing a crash.
		//The returned function should not actually be blank, this would cause a crash when the host program tries to use it.
		//Instead, the error messages show that something went wrong, and the function simply does nothing.
		return std::function<double(std::unordered_map<std::string,double>&)>( [](std::unordered_map<std::string,double>&) {
			return 0.0;
		} );
	}

	//The next step is to lex the prepared string.
	//This involves identifying which groups of characters form meaningful units, and then separating and labelling them.
	// e.g. "x+1" could become "Variable: x, Operator: +, Number: 1"
	std::queue<LexToken> lexed = m_Lexer ( preprocessed );
	if ( !m_Lexer.getStatus() ) {
		m_LogError ( "Lexing failed" );
		m_LogError ( "Lexed tokens: ");
		while ( !lexed.empty() ) {
			m_LogError ( lexed.front().getLexeme() );
			lexed.pop();
		}
		return std::function<double(std::unordered_map<std::string,double>&)>( [](std::unordered_map<std::string,double>&) {
			return 0.0;
		} );
	}

	//The third step is to convert this series of tokens from infix to postfix notation.
	//This is done via a slightly modified version of Djiksta's Shunting Yard algorithm, hence the name of the class.
	//It uses an internal stack to hold some tokens while others are moved directly to the output.
	//If done correctly, this reorders them into what is effictively a flat representation of a tree structure.
	std::stack<ShuntingToken> shunted = m_ShuntingYard ( lexed );
	if ( !m_ShuntingYard.getStatus() ) {
		m_LogError ( "Shunting failed" );
		m_LogError ( "Shunted tokens:" );
		while ( !shunted.empty() ) {
			m_LogError ( shunted.top().getLexeme() );
			shunted.pop();
		}
		return std::function<double(std::unordered_map<std::string,double>&)>( [](std::unordered_map<std::string,double>&) {
			return 0.0;
		} );
	}

	//The penultimate step is to turn this postfix expression into an actual tree.
	//This then allows everything to be handled by virtual functions in the nodes and calculated recursively in the tree.
	auto parsed = m_Parser ( shunted );
	if ( !m_Parser.getStatus() ) {
		m_LogError ( "Parsing failed" );
		return std::function<double(std::unordered_map<std::string,double>&)>( [](std::unordered_map<std::string,double>&) {
			return 0.0;
		} );
	}

	//Finally, the tree must be optimised and converted to a single function.
	//Simplify() applies recursive optimisation operations such as calculating the value of constant subtrees and replacing them
	//with a single node.
	//GetFunction() is another recursive function that creates an std::function from the results of calling GetFunction() on each
	//node's child nodes. The result is a single function that produces the same result as recursively evaluating the tree.
	return parsed->simplify()->getFunction();
}

//Differentiate expression with respect to variable.
//This returns a string rather than a function so that the host program can decide whether it wants a differentiated expression,
//a differentiated function, or both. The main compile() function can be used in conjunction with this one to produce an actual
//differentiated function.
std::string Compiler::differentiate ( std::string expression, std::string variable )
{
	//The steps are exactly the same as compiling, up until the end.
	std::string preprocessed = m_Preprocessor ( expression );
	if ( !m_Preprocessor.getStatus() ) {
		m_LogError ( "Preprocessing failed" );
		m_LogError ( "Preprocessed string:" );
		m_LogError ( preprocessed );
		return std::string();
	}

	std::queue<LexToken> lexed = m_Lexer ( preprocessed );
	if ( !m_Lexer.getStatus() ) {
		m_LogError ( "Lexing failed" );
		m_LogError ( "Lexed tokens: ");
		while ( !lexed.empty() ) {
			m_LogError ( lexed.front().getLexeme() );
			lexed.pop();
		}
		return std::string();
	}

	std::stack<ShuntingToken> shunted = m_ShuntingYard ( lexed );
	if ( !m_ShuntingYard.getStatus() ) {
		m_LogError ( "Shunting failed" );
		m_LogError ( "Shunted tokens:" );
		while ( !shunted.empty() ) {
			m_LogError ( shunted.top().getLexeme() );
			shunted.pop();
		}
		return std::string();
	}

	auto parsed = m_Parser ( shunted );
	if ( !m_Parser.getStatus() ) {
		m_LogError ( "Parsing failed" );
		return std::string();
	}

	//This is where this function differs from compile(). Instead of returning the function, it differentiates the tree, simplifies
	//it, and then converts it back to an expression which will produce exactly the same tree if compiled.
	//Differentiation is another recursive process, as the differential of any function can be created from the original and
	//differentiated versions of its operands.
	return parsed->simplify()->getDifferential( variable )->simplify()->getName();
}

//This allows the host program to correctly create and populate the map of variable name to value that is passed to functions
//produced by compile().
std::set<std::string> Compiler::getVariableNames ( std::string expression )
{
	//This time, the expression only needs to get as far as being lexed.
	std::string preprocessed = m_Preprocessor ( expression );
	if ( !m_Preprocessor.getStatus() ) {
		m_LogError ( "Preprocessing failed" );
		m_LogError ( "Preprocessed string:" );
		m_LogError ( preprocessed );
		return std::set<std::string>();
	}
	std::queue<LexToken> lexed = m_Lexer ( preprocessed );
	if ( !m_Lexer.getStatus() ) {
		m_LogError ( "Lexing failed" );
		m_LogError ( "Lexed tokens: ");
		while ( !lexed.empty() ) {
			m_LogError ( lexed.front().getLexeme() );
			lexed.pop();
		}
		return std::set<std::string>();
	}

	//Now that the expression is lexed, all the variables have been identified and can be easily picked out from the queue.
	std::set<std::string> variableNames;

	while ( !lexed.empty() ) {
		if ( lexed.front().getType() == LexToken::Type::Variable ) {
			//There's no need to check if this variable has already been found, as set only stores things once.
			variableNames.insert ( lexed.front().getLexeme() );
		}
		lexed.pop();
	}

	return variableNames;
}

//The following functions are wrapper functions that allow the host program to customise the inner workings of the compiler.
//Adding a constant to the compiler means that when it is found in an expression, it will be replaced with the given value.
void Compiler::addConstant ( std::string name, double value )
{
	m_Preprocessor.addConstant ( name, value );
}

void Compiler::removeConstant ( std::string name )
{
	m_Preprocessor.removeConstant ( name );
}

//All the functions that the compiler initially understands are hardcoded in, usually as wrappers around cmath functions.
//However the user can add their own functions made up of these predefined building blocks.
//In order to do this, the preprocessor has to know to mark these new function names, and the parser has to know the replace them
//with subtrees created from their definition.
void Compiler::addFunction ( std::string name, std::string variable, std::string expression )
{
	//This is the simple bit, the preprocessor already replaces functions names like "sin" with "_sin_", so that the lexer knows
	//that they are functions. It simply has to be told that if the user has defined f(x), "f" must be replaced with "_f_".
	m_Preprocessor.addFunction ( name );

	//The parser requries a shunted version of the function in order to create a subtree, so the first few steps of the compilation
	//process are used again here.
	std::string preprocessed = m_Preprocessor ( expression );
	if ( !m_Preprocessor.getStatus() ) {
		m_LogError ( "Preprocessing failed" );
		m_LogError ( "Preprocessed string:" );
		m_LogError ( preprocessed );
		return;
	}

	std::queue<LexToken> lexed = m_Lexer ( preprocessed );
	if ( !m_Lexer.getStatus() ) {
		m_LogError ( "Lexing failed" );
		m_LogError ( "Lexed tokens: ");
		while ( !lexed.empty() ) {
			m_LogError ( lexed.front().getLexeme() );
		}
		return;
	}

	std::stack<ShuntingToken> shunted = m_ShuntingYard ( lexed );
	if ( !m_ShuntingYard.getStatus() ) {
		m_LogError ( "Shunting failed" );
		m_LogError ( "Shunted tokens:" );
		while ( !shunted.empty() ) {
			m_LogError ( shunted.top().getLexeme() );
		}
		return;
	}

	//The parser needs to know what the function is called so that it can spot it, which variable to replace with the expression
	//passed to the function, and the definition of the function itself.
	m_Parser.addFunction ( name, variable, shunted );
}

void Compiler::removeFunction ( std::string name )
{
	m_Preprocessor.removeFunction( name );
	m_Parser.removeFunction( name  );
}

//This allows the user to decide whether or not they want to use degrees, radians, or gradians.
//Unfortunately, calculus only works in radians, because otherwise you'd have to change precompiled functions when you change the
//DRG mode.
void Compiler::setTrigMode ( DRG mode )
{
	//For ease of use for whoever is writing the host program, Compiler has to expose its own DRG enum, so for this to be passed to
	//ParseNode, it has to be converted to ParseNode's enum.
	switch ( mode ) {
	case DRG::Degrees:
		ParseNode::setDRGMode ( ParseNode::DRG::Degrees );
		break;

	case DRG::Radians:
		ParseNode::setDRGMode ( ParseNode::DRG::Radians );
		break;

	case DRG::Gradians:
		ParseNode::setDRGMode ( ParseNode::DRG::Gradians );
		break;
	};
}

}
