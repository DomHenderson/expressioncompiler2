//STANDARD LIBRARY INCLUDES
#include <string> //std::string

//INTERNAL INCLUDES
#include "operatorToken.hpp" //ec::OperatorToken

namespace ec {

OperatorToken::OperatorToken ( Type type, std::string lexeme ) :
	m_Type ( type ),
	m_Lexeme ( lexeme )
{
}

OperatorToken::Type OperatorToken::getType() const
{
	return m_Type;
}

const std::string& OperatorToken::getLexeme() const
{
	return m_Lexeme;
}

}
