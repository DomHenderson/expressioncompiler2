//STANDARD LIBRARY INCLUDES
#include <iostream> //std::cout, std::endl
#include <memory>   //std::make_unique, std::unique_ptr
#include <queue>    //std::queue
#include <string>   //std::string
#include <utility>  //std::move

//INTERNAL INCLUDES
#include "parseNode.hpp"  //ec::ParseNode
#include "parser.hpp"     //ec::Parser
#include "parseToken.hpp" //ec::ParseToken

namespace ec {

ParameterParseToken::ParameterParseToken ( std::string name ) :
	m_Name ( name )
{
}

std::unique_ptr<ParseNode> ParameterParseToken::getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& ) //Unused parameter
{
	return std::make_unique<ParameterParseNode>( m_Name );
}

void ParameterParseToken::print () const
{
	std::cout<<"Parameter of name: "<<m_Name<<std::endl;
}

ConstantParseToken::ConstantParseToken ( double value ):
	m_Value ( value )
{
}

std::unique_ptr<ParseNode> ConstantParseToken::getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& ) //Unused parameter
{
	return std::make_unique<ConstantParseNode> ( m_Value );
}

void ConstantParseToken::print() const
{
	std::cout<<"Constant of value: "<<m_Value<<std::endl;
}

OperationParseToken::OperationParseToken ( Type type ):
	m_Type ( type )
{
}

std::unique_ptr<ParseNode> OperationParseToken::getNodePtr ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	//The held enum must be matched to the appropriate derived ParseNode.
	//The rest of the ParseTokens are then passed to the ParseNode so that it can create its operands and the recursion continues.
	switch ( m_Type ) {
	case Type::Addition:
		return std::make_unique<AdditionParseNode> ( tokens );
		break;

	case Type::Subtraction:
		return std::make_unique<SubtractionParseNode> ( tokens );
		break;

	case Type::Multiplication:
		return std::make_unique<MultiplicationParseNode> ( tokens );
		break;

	case Type::Division:
		return std::make_unique<DivisionParseNode> ( tokens );
		break;

	case Type::Power:
		return std::make_unique<PowerParseNode> ( tokens );
		break;

	case Type::Negation:
		return std::make_unique<NegationParseNode> ( tokens );
		break;

	case Type::Acosech:
		return std::make_unique<AcosechParseNode> ( tokens );
		break;

	case Type::Acosec:
		return std::make_unique<AcosecParseNode> ( tokens );
		break;

	case Type::Cosech:
		return std::make_unique<CosechParseNode> ( tokens );
		break;

	case Type::Acosh:
		return std::make_unique<AcoshParseNode> ( tokens );
		break;

	case Type::Acoth:
		return std::make_unique<AcothParseNode> ( tokens );
		break;

	case Type::Asech:
		return std::make_unique<AsechParseNode> ( tokens );
		break;

	case Type::Asinh:
		return std::make_unique<AsinhParseNode> ( tokens );
		break;

	case Type::Atanh:
		return std::make_unique<AtanhParseNode> ( tokens );
		break;

	case Type::Cosec:
		return std::make_unique<CosecParseNode> ( tokens );
		break;

	case Type::Acos:
		return std::make_unique<AcosParseNode> ( tokens );
		break;

	case Type::Acot:
		return std::make_unique<AcotParseNode> ( tokens );
		break;

	case Type::Asec:
		return std::make_unique<AsecParseNode> ( tokens );
		break;

	case Type::Asin:
		return std::make_unique<AsinParseNode> ( tokens );
		break;

	case Type::Atan:
		return std::make_unique<AtanParseNode> ( tokens );
		break;

	case Type::Cosh:
		return std::make_unique<CoshParseNode> ( tokens );
		break;

	case Type::Coth:
		return std::make_unique<CothParseNode> ( tokens );
		break;

	case Type::Log2:
		return std::make_unique<Log2ParseNode> ( tokens );
		break;

	case Type::Sech:
		return std::make_unique<SechParseNode> ( tokens );
		break;

	case Type::Sinh:
		return std::make_unique<SinhParseNode> ( tokens );
		break;

	case Type::Tanh:
		return std::make_unique<TanhParseNode> ( tokens );
		break;

	case Type::Abs:
		return std::make_unique<AbsParseNode> ( tokens );
		break;

	case Type::Cos:
		return std::make_unique<CosParseNode> ( tokens );
		break;

	case Type::Cot:
		return std::make_unique<CotParseNode> ( tokens );
		break;

	case Type::Exp:
		return std::make_unique<ExpParseNode> ( tokens );
		break;

	case Type::Log:
		return std::make_unique<LogParseNode> ( tokens );
		break;

	case Type::Sec:
		return std::make_unique<SecParseNode> ( tokens );
		break;

	case Type::Sin:
		return std::make_unique<SinParseNode> ( tokens );
		break;

	case Type::Tan:
		return std::make_unique<TanParseNode> ( tokens );
		break;

	case Type::Ln:
		return std::make_unique<LnParseNode> ( tokens );
		break;

	default:
		return std::unique_ptr<ParseNode> (); //In theory, error information could also be held in ParseNodes. However in practice,
		break;                                //the held information would be of such little use, and would require such effort to
	}                                         //code that it's not worth it, especially as few errors will get to this stage, and
	                                          //even if they do, they're never hard to spot as long as the user knows there's an error.
}

void OperationParseToken::print() const
{
	std::cout<<"Operation of type: ";
	switch ( m_Type ) {
	case Type::Abs:
		std::cout<<"Abs";
		break;

	case Type::Acos:
		std::cout<<"Acos";
		break;

	case Type::Acosec:
		std::cout<<"Acosec";
		break;

	case Type::Acosech:
		std::cout<<"Acosech";
		break;

	case Type::Acosh:
		std::cout<<"Acosh";
		break;

	case Type::Acot:
		std::cout<<"Acot";
		break;

	case Type::Acoth:
		std::cout<<"Acoth";
		break;

	case Type::Addition:
		std::cout<<"Addition";
		break;

	case Type::Asec:
		std::cout<<"Asec";
		break;

	case Type::Asech:
		std::cout<<"Asech";
		break;

	case Type::Asin:
		std::cout<<"Asin";
		break;

	case Type::Asinh:
		std::cout<<"Asinh";
		break;

	case Type::Atan:
		std::cout<<"Atan";
		break;

	case Type::Atanh:
		std::cout<<"Atanh";
		break;

	case Type::Cos:
		std::cout<<"Cos";
		break;

	case Type::Cosec:
		std::cout<<"Cosec";
		break;

	case Type::Cosech:
		std::cout<<"Cosech";
		break;

	case Type::Cosh:
		std::cout<<"Cosh";
		break;

	case Type::Cot:
		std::cout<<"Cot";
		break;

	case Type::Coth:
		std::cout<<"Coth";
		break;

	case Type::Division:
		std::cout<<"Division";
		break;

	case Type::Exp:
		std::cout<<"Exp";
		break;

	case Type::Ln:
		std::cout<<"Ln";
		break;

	case Type::Log:
		std::cout<<"Log";
		break;

	case Type::Log2:
		std::cout<<"Log2";
		break;

	case Type::Multiplication:
		std::cout<<"Multiplication";
		break;

	case Type::Negation:
		std::cout<<"Negation";
		break;

	case Type::Power:
		std::cout<<"Power";
		break;

	case Type::Sec:
		std::cout<<"Sec";
		break;

	case Type::Sech:
		std::cout<<"Sech";
		break;

	case Type::Sin:
		std::cout<<"Sin";
		break;

	case Type::Sinh:
		std::cout<<"Sinh";
		break;

	case Type::Subtraction:
		std::cout<<"Subtraction";
		break;

	case Type::Tan:
		std::cout<<"Tan";
		break;

	case Type::Tanh:
		std::cout<<"Tanh";
		break;
	}

	std::cout<<std::endl;
}

CustomFunctionParseToken::CustomFunctionParseToken( std::string name, Parser& parser ) :
	m_Parser ( parser ),
	m_Name ( name )
{
}

ParseNode::Ptr CustomFunctionParseToken::getNodePtr( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	//The parser is used to convert the custom function into an actual tree of nodes that can be evaluated.
	return m_Parser.getCustomFunctionTree ( m_Name, tokens );
}

void CustomFunctionParseToken::print() const
{
	std::cout<<m_Name<<std::endl;
}

CustomFunctionInputParseToken::CustomFunctionInputParseToken( ParseNode::Ptr node ) :
	m_Node ( std::move ( node ) )
{
}

ParseNode::Ptr CustomFunctionInputParseToken::getNodePtr( std::queue<std::unique_ptr<ParseToken>>& )
{
	//getCopy is used so that the token doesn't lose ownership of its node
	return m_Node->getCopy();
}

void CustomFunctionInputParseToken::print() const
{
	std::cout<<"Input: "<<m_Node->getName()<<std::endl;
}

}
