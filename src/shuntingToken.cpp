//STANDARD LIBRARY INCLUDES
#include <iostream> //std::cout, std::endl
#include <string>   //std::string

//INTERNAL INCLUDES
#include "shuntingToken.hpp" //ec::ShuntingToken

namespace ec {

ShuntingToken::ShuntingToken ( const std::string& lexeme, Type type) :
	m_Lexeme ( lexeme ),
	m_Type ( type )
{
}

ShuntingToken::Type ShuntingToken::getType() const
{
	return m_Type;
}

const std::string& ShuntingToken::getLexeme() const
{
	return m_Lexeme;
}

void ShuntingToken::print()
{
	switch ( m_Type ) {
		case Type::Function:
			std::cout<<"Function : ";
			break;

		case Type::BinaryOperator:
			std::cout<<"Binary operator : ";
			break;

		case Type::UnaryOperator:
			std::cout<<"Unary operator : ";
			break;

		case Type::Number:
			std::cout<<"Number : ";
			break;

		case Type::Variable:
			std::cout<<"Variable : ";
			break;

		case Type::Error:
			std::cout<<"Error : ";
			break;
		}

		std::cout<<m_Lexeme<<std::endl;
}

}
