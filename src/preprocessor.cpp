//STANDARD LIBRARY INCLUDES
#include <algorithm> //std::any_of, std::count, std::remove_if
#include <iomanip>   //std::setprecision
#include <utility>   //std::pair
#include <sstream>   //std::stringstream
#include <string>    //std::string

//INTERNAL INCLUDES
#include "classifier.hpp"   //ec::Classifier
#include "preprocessor.hpp" //ec::Preprocessor

namespace ec {

//These functions could be implemented inline, however it's easier to read the code if they are brought out here and defined as local functions.
namespace {
	bool IsIllegalChar ( char c ) {
		return Classifier::Classify ( c ) == Classifier::Type::Bad;
	}

	bool ShouldBeRemoved ( char c ) {
		return Classifier::Classify ( c ) == Classifier::Type::Bad || Classifier::Classify ( c ) == Classifier::Type::Whitespace;
	}

	bool ImpliedMultiplication ( char first, char second ) {
		if ( Classifier::Classify ( first ) == Classifier::Type::Number ) {
			return Classifier::Classify ( second ) == Classifier::Type::Letter ||
			       second == '_' || second == '(';
		} else if ( Classifier::Classify ( first ) == Classifier::Type::Letter ) {
			return Classifier::Classify ( second ) == Classifier::Type::Number ||
			       Classifier::Classify ( second ) == Classifier::Type::Letter ||
			       second == '_' || second == '(';
		} else if ( first == ')' ) {
			return Classifier::Classify ( second ) == Classifier::Type::Number ||
			       Classifier::Classify ( second ) == Classifier::Type::Letter ||
			       second == '_' || second == '(';
		} else {
			return false;
		}
	}
}

Preprocessor::Preprocessor() :
	//Status is false by default, because nothing has gone wrong yet
	m_Status ( true ),
	//Strings are defined in length order so that, for example, it checks from asin before sin.
	//Each group of a particular length is then defined in alphabetical order because I'm a bit sad.
	m_Replacements ( {
		{ "acosech", "_acosech_" },
		{ "acosec", "_acosec_" },
		{ "cosech","_cosech_" },
		{ "acosh", "_acosh_" },
		{ "acoth", "_acoth_" },
		{ "asech", "_asech_" },
		{ "asinh", "_asinh_" },
		{ "atanh", "_atanh_" },
		{ "cosec", "_cosec_" },
		{ "acos", "_acos_" },
		{ "acot", "_acot_" },
		{ "asec", "_asec_" },
		{ "asin", "_asin_" },
		{ "atan", "_atan_" },
		{ "cosh", "_cosh_" },
		{ "coth", "_coth_" },
		{ "log2", "_log2_" },
		{ "sech", "_sech_" },
		{ "sinh", "_sinh_" },
		{ "tanh", "_tanh_" },
		{ "abs", "_abs_" },
		{ "cos", "_cos_" },
		{ "cot", "_cot_" },
		{ "exp", "_exp_" },
		{ "log", "_log_" },
		{ "sec", "_sec_" },
		{ "sin", "_sin_" },
		{ "tan", "_tan_" },
		{ "ln",  "_ln_" }
	} )
{
}

std::string Preprocessor::operator() ( const std::string& expression )
{
	//Until the preprocessor hits an error, its status should be true.
	m_Status = true;

	//These expressions check for errors in the string.
	CheckNotBlank ( expression );
	FindIllegalCharacters ( expression );

	//If one is found, stop now and inform the compiler.
	if ( !m_Status ) {
		return expression;
	}

	//This is created because expression is a const reference, and the following functions all modify the string.
	std::string processed ( expression );

	RemoveIgnoredCharacters ( processed ); //Whitespace needs to be removed.
	MakeReplacements ( processed ); //Constants need to be replaced with values and functions need to be labelled.
	ReplaceUnaryNegation ( processed ); //Unary and binary negation are different operations, so one needs to use a different symbol.
	InsertImpliedMultiplication ( processed ); //Implied multiplication, such as in "x(x+1)", needs to be made explicit so that the lexer will notice it.

	return processed;

}

bool Preprocessor::getStatus()
{
	return m_Status;
}

void Preprocessor::addConstant ( std::string constantName, double value )
{
	//This method is used rather than std::to_string, because to_string only uses 6 decimal places, whereas stringstreams can use any number.
	std::string valueString;
	std::stringstream ss;
	ss << std::setprecision ( 15 ) << value; //15 decimal places is the precision of a double. Any more would be pointless.
	ss >> valueString;

	//Because of the custom KeyComparator functor, items can simply be inserted into the map.
	m_Replacements[constantName] = valueString;
}

void Preprocessor::removeConstant ( std::string constantName )
{
	m_Replacements.erase ( constantName );
}

void Preprocessor::addFunction ( std::string name )
{
	m_Replacements[name] = "_" + name + "_";
}

void Preprocessor::removeFunction ( std::string name )
{
	m_Replacements.erase ( name );
}

void Preprocessor::FindIllegalCharacters ( const std::string& expression )
{
	//IsIllegalChar is defined separately for legibility.
	if ( std::any_of ( expression.begin(), expression.end(), IsIllegalChar ) ) {
		m_Status = false;
	}
}

void Preprocessor::CheckNotBlank ( const std::string& expression )
{
	//Without this function, preprocessing "" causes a segfault.
	//It also makes sense because "" is invalid input to the compiler anyway.
	if ( expression == "" ) {
		m_Status = false;
	}
}

void Preprocessor::RemoveIgnoredCharacters ( std::string& expression )
{
	//Removing whitespace makes the lexer's job a lot easier. It can possibly introduce some slightly weird resutls, but only from
	//very weird inputs. e.g. "s in(x)" will still be interpreted as "sin(x)".
	expression.erase (
		std::remove_if (
			expression.begin(),
			expression.end(),
			ShouldBeRemoved
		),
		expression.end()
	);
}

void Preprocessor::MakeReplacements ( std::string& expression )
{
	//All the replacements are stored in a vector of pairs so that this can be done generically.
	//The first element of each pair is the original string, and the second is the string that replaces it.
	for ( auto& name: m_Replacements ) {
		std::size_t foundPos = expression.find ( name.first );

		while ( foundPos != std::string::npos ) {
			if ( std::count ( expression.begin(), expression.begin() + foundPos + 1, '_' ) % 2 == 0 ) {
				expression.replace ( foundPos, name.first.length(), name.second );
			}

			foundPos = expression.find ( name.first, foundPos + name.second.length() );
		}
	}
}

//This is done because it makes it much easier for the rest of compilation as unary and binary negation are technically different functions.
void Preprocessor::ReplaceUnaryNegation ( std::string& expression )
{
	if ( expression[0] == '-' ) {
		expression[0] = '~';
	}

	for ( unsigned i ( 1 ); i < expression.length() - 1; ++i ) {
		if (
			expression[i] == '-' && expression[i-1] != ')' &&
			Classifier::Classify( expression[i-1] ) != Classifier::Type::Number &&
			Classifier::Classify( expression[i-1] ) != Classifier::Type::Letter
		) {
			expression[i] = '~';
		}
	}
}

//This is needed because there needs to be a multiplication token where one has been implied, but the lexer will only add one if there
//is a multiplication sign.
void Preprocessor::InsertImpliedMultiplication( std::string& expression ) {
	for ( unsigned i ( 0 ); i < expression.length() - 1; ++i ) {
		if ( expression[i] == '_' ) {
			i = expression.find ( "_", i + 1 );
		} else if ( ImpliedMultiplication(expression[i], expression[i+1]) ) {
			expression.insert ( i + 1, "*" );
		}
	}
}

}
