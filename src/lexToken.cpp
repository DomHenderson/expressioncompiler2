//STANDARD LIBRARY INCLUDES
#include <iostream> //std::cout, std::endl
#include <string>   //std::string

//INTERNAL INCLUDES
#include "lexToken.hpp" //ec::LexToken

namespace ec {

LexToken::LexToken ( const std::string& lexeme, Type type ) :
	m_Lexeme ( lexeme ),
	m_Type ( type )
{
}

void LexToken::print () {

	//The only ways to convert an enum to a string are with a map, a switch, or a cascade of ifs.
	//It's not the shortest and nicest to look at, but it's very simple to read and efficient.
	switch ( m_Type ) {
	case Type::Function:
		std::cout<<"Function : ";
		break;

	case Type::BinaryOperator:
		std::cout<<"Binary operator : ";
		break;

	case Type::UnaryOperator:
		std::cout<<"Unary operator : ";
		break;

	case Type::Number:
		std::cout<<"Number : ";
		break;

	case Type::Variable:
		std::cout<<"Variable : ";
		break;

	case Type::Bracket:
		std::cout<<"Bracket : ";
		break;

	case Type::Separator:
		std::cout<<"Separator : ";
		break;
	}

	//No matter what the type is, the output is in the form "Type : lexeme", so the lexeme can be printed after the switch.
	std::cout<<m_Lexeme<<std::endl;
}

LexToken::Type LexToken::getType() const {
	return m_Type;
}

const std::string& LexToken::getLexeme() const {
	return m_Lexeme;
}

}
