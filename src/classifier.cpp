//INTERNAL INCLUDES
#include "classifier.hpp" //ec::Classifier

//C++17 allows nested namespaces to be declared inline
namespace ec::Classifier {

Type Classify ( char character )
{
	//Character values are arranged sequentially (char uses extended ASCII values).
	//Therefore if a character is a letter, its value will fall between either 'a' and 'z', or 'A' and 'Z'.
	//The same logic can be applied for numbers, in that they will have a value between that of '0' and '9'.
	if ( ( character >= 'a' && character <= 'z' ) || ( character >= 'A' && character <= 'Z' ) ) {
		return Type::Letter;
	} else if ( character >= '0' && character <= '9' ) {
		return Type::Number;
	} else if ( character == '.' ) {
		return Type::Point;
	} else if ( //Because operators are not all grouped together, they have to be checked individually.
		character == '+' || character == '-' || character == '*' ||
		character == '/' || character == '^' || character == '~' //~ is a replacement for a unary - so the lexer can tell them apart.
	) {
		return Type::Operator;
	} else if ( character == '(' || character == ')' ) {
		return Type::Bracket;
	} else if ( character == ' ' || character == '	' ) {
		return Type::Whitespace;
	} else {
		return Type::Bad; //The above are the only characters that should be in an expression, so otherwise it must be an error.
	}
}

}
