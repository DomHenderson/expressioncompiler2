//STANDARD LIBRARY INCLUDES
#include <iostream> //std::cout, std::endl
#include <stack>    //std::stack

//INTERNAL INCLUDES
#include "lexToken.hpp"      //ec::LexToken
#include "operatorToken.hpp" //ec::OperatorToken
#include "shuntingToken.hpp" //ec::ShuntingToken
#include "shuntingYard.hpp"  //ec::ShuntingYard

namespace ec {

ShuntingYard::OperatorStack::OperatorStack( std::stack<ShuntingToken>& output ) :
	m_Output ( output ), m_Status ( true )
{
}

bool ShuntingYard::OperatorStack::getStatus() const
{
	return m_Status;
}

namespace {
	//ShuntingTokens are slightly different to OperatorTokens in that OperatorTokens can be brackets, whereas ShuntingTokens can't.
	//As ShuntingToken's therefore need their own Type enum, there must be a switch statement to convert from one to the other.
	ShuntingToken CreateShuntingToken ( OperatorToken token ) {
		switch ( token.getType() ) {
		case OperatorToken::Type::BinaryOperator:
			return ShuntingToken ( token.getLexeme(), ShuntingToken::Type::BinaryOperator );
			break;

		case OperatorToken::Type::Function:
			return ShuntingToken ( token.getLexeme(), ShuntingToken::Type::Function );
			break;

		case OperatorToken::Type::UnaryOperator:
			return ShuntingToken ( token.getLexeme(), ShuntingToken::Type::UnaryOperator );
			break;

		//Brackets should not make it through the OperatorStack, instead they should be used to determine the flow of other tokens
		//and discarded once they've been used.
		case OperatorToken::Type::OpeningBracket:
			return ShuntingToken ( "Opening bracket output from OperatorStack", ShuntingToken::Type::Error );
			break;

		case OperatorToken::Type::ClosingBracket:
			return ShuntingToken ( "Closing bracket output OperatorStack", ShuntingToken::Type::Error );
			break;

		case OperatorToken::Type::Error:
			return ShuntingToken ( token.getLexeme(), ShuntingToken::Type::Error );
			break;

		default:
			return ShuntingToken ( "Attempt to convert OperatorToken with unknown Type to ShuntingToken. Token contained: " + token.getLexeme(), ShuntingToken::Type::Error );
			break;
		}
	}
}

void ShuntingYard::OperatorStack::push ( LexToken token )
{
	m_Status = true; //If none of the private member functions set this to false, this function will have been successful
	OperatorToken opToken = CreateOperatorToken ( token );

	if ( opToken.getType() == OperatorToken::Type::Error ) {
		m_Status = false;
		return; //Returning now will leave m_Status as false, and so it can be worked out that something went wrong
	}

	//Part of the Shunting Yard algorithm is that before tokens are pushed onto the operator stack, tokens that should come after
	//them in the final output must be pushed to the output. Hence why this only needs to be done when the internal stack is not empty.
	if ( !m_Stack.empty() ) {
		PrepareForPush ( opToken );
	}

	//Closing brackets trigger the popping of all tokens until an opening bracket is found, they are then discarded.
	//Any other tokens are simply added the the internal stack.
	if ( opToken.getLexeme() != ")" ) {
		m_Stack.push ( opToken );
	}
}

OperatorToken ShuntingYard::OperatorStack::CreateOperatorToken ( LexToken token )
{
	//Another conversion switch, this time from LexToken to OperatorToken.
	//The difference between these two is that some tokens should never be passed to the OperatorStack, they should go straight to
	//the ShuntingYard's output, and hence OperatorTokens cannot be numbers, variables, or separators. Opening and closing brackets
	//are also completely different to the OperatorStack, so for simplicity, they are split now.
	switch ( token.getType() ) {
	case LexToken::Type::BinaryOperator:
		return OperatorToken ( OperatorToken::Type::BinaryOperator, token.getLexeme() );
		break;

	case LexToken::Type::Bracket:
		if ( token.getLexeme() == "(" ) {
			return OperatorToken ( OperatorToken::Type::OpeningBracket, token.getLexeme() );
		} else {
			return OperatorToken ( OperatorToken::Type::ClosingBracket, token.getLexeme() );
		}
		break;

	case LexToken::Type::Function:
		return OperatorToken ( OperatorToken::Type::Function, token.getLexeme() );
		break;

	case LexToken::Type::UnaryOperator:
		return OperatorToken ( OperatorToken::Type::UnaryOperator, token.getLexeme() );
		break;

	case LexToken::Type::Number:
		return OperatorToken ( OperatorToken::Type::Error, "Number passed to OperatorStack" );
		break;

	case LexToken::Type::Variable:
		return OperatorToken ( OperatorToken::Type::Error, "Variable passed to OperatorStack" );
		break;

	case LexToken::Type::Separator:
		return OperatorToken ( OperatorToken::Type::Error, "Separator passed to OperatorStack" );
		break;

	default:
		return OperatorToken ( OperatorToken::Type::Error, "Attempt to convert LexToken of unknown Type to OperatorToken. Token contained: " + token.getLexeme() );
		break;
	}
}

void ShuntingYard::OperatorStack::PrepareForPush ( OperatorToken token )
{
	//There are two possible reasons why the internal stack would not be ready to recieve the next token.
	//If the next token is a closing bracket, it must trigger HandleClosingBracket and then be discarded.
	//If a token of higher priority than the one being pushed is on top of the stack, it must be moved to the output.
	//This is determined by both order of operations and associativity of operators, so it is very lengthy, as each case needs to be
	//handled individually. Or at least it's a lot easier to do that than program it generically.
	if ( token.getType() == OperatorToken::Type::ClosingBracket ) {
		HandleClosingBracket();
	} else {
		while ( !ReadyForPush( token ) ) {
			PopToOutput();
		}
	}
}

void ShuntingYard::OperatorStack::HandleClosingBracket()
{
	//Anything surrounded by brackets is automatically of a higher priority, and so must be removed from the internal stack when the
	//closing bracket is reached
	while ( !m_Stack.empty() && m_Stack.top().getType() != OperatorToken::Type::OpeningBracket ) {
		PopToOutput();
	}
	if ( m_Stack.empty() ) {
		m_Status = false;
		m_Stack.push ( OperatorToken ( OperatorToken::Type::Error, "Mismatched closing bracket" ) );
		return;
	}
	//The opening bracket is then removed as its purpose is also done and it should not end up in ShuntingYard's output.
	if ( m_Stack.top().getType() == OperatorToken::Type::OpeningBracket ) {
		m_Stack.pop();
	}
}

bool ShuntingYard::OperatorStack::ReadyForPush( OperatorToken token )
{
	//This is a very long and quite difficult to read function, however it's purpose is relatively simple.
	//If the token on top of the internal stack has a higher priority than the one that it being pushed to the stack, the one on top
	//of the stack should be pushed to the output.
	if ( m_Stack.empty() ) {
		return true;
	} else if ( m_Stack.top().getType() == OperatorToken::Type::OpeningBracket && token.getType() != OperatorToken::Type::Error ) {
		return true;
	} else {
		switch ( token.getType() ) {
		case OperatorToken::Type::BinaryOperator:
			switch ( m_Stack.top().getType() ) {
			case OperatorToken::Type::OpeningBracket:
				return true;
				break;

			case OperatorToken::Type::ClosingBracket:
				m_Stack.push ( OperatorToken( OperatorToken::Type::Error, "Closing bracket found in OperatorStack's inner stack" ) );
				popAll();
				m_Status = false;
				return true;
				break;

			case OperatorToken::Type::Error:
				return false;
				break;

			case OperatorToken::Type::Function:
				return false;
				break;

			case OperatorToken::Type::UnaryOperator:
				if ( m_Stack.top().getLexeme() == "~" ) {
					return token.getLexeme() != "+" && token.getLexeme() != "-";
				} else {
					m_Stack.push ( OperatorToken ( OperatorToken::Type::Error, "Unknown unary operator on OperatorStack: " + m_Stack.top().getLexeme() ) );
					popAll();
					m_Status = false;
					return true;
				}
				break;

			case OperatorToken::Type::BinaryOperator:
				if ( m_Stack.top().getLexeme() == "^" ) {
					return token.getLexeme() == "^";
				} else if ( m_Stack.top().getLexeme() == "*" || m_Stack.top().getLexeme() == "/" ) {
					return token.getLexeme() == "^";
				} else if ( m_Stack.top().getLexeme() == "+" || m_Stack.top().getLexeme() == "-" ) {
					return token.getLexeme() != "+" && token.getLexeme() != "-";
				} else {
					m_Stack.push ( OperatorToken ( OperatorToken::Type::Error, "Unknown binary operator on OperatorStack: " + m_Stack.top().getLexeme() ) );
					popAll();
					m_Status = false;
					return true;
				}
				break;

			default:
				m_Stack.push ( OperatorToken ( OperatorToken::Type::Error, "OperatorToken of unknown Type on OperatorStack. Token contained: " + token.getLexeme() ) );
				popAll();
				m_Status = false;
				return true;
				break;
			}
			break;

		case OperatorToken::Type::Function:
		case OperatorToken::Type::UnaryOperator:
		case OperatorToken::Type::OpeningBracket:
			return true;
			break;

		case OperatorToken::Type::ClosingBracket:
			m_Stack.push ( OperatorToken ( OperatorToken::Type::Error, "Attempt to push closing bracket to OperatorStack not handled correctly" ) );
			popAll();
			m_Status = false;
			return true;
			break;

		case OperatorToken::Type::Error:
			m_Stack.push ( OperatorToken ( OperatorToken::Type::Error, "Error token pushed generically to OperatorStack. Error: " + token.getLexeme() ) );
			popAll();
			m_Status = false;
			return true;
			break;

		default:
			m_Stack.push ( OperatorToken ( OperatorToken::Type::Error, "Attempt to push OperatorToken of unknown Type. Token contained: " + token.getLexeme() ) );
			popAll();
			m_Status = false;
			return true;
			break;
		}
	}
}

//This function exists for convenience as an enum conversion has to happen whenever a token is moved from the internal stack to the
//output.
void ShuntingYard::OperatorStack::PopToOutput()
{
	std::cout<<"PopToOutput: "<<m_Stack.top().getLexeme()<<std::endl;
	m_Output.push ( CreateShuntingToken ( m_Stack.top() ) );
	m_Stack.pop();
}

//When the ShuntingYard has no more tokens to pass to the OperatorStack, those remaining in the internal stack must be pushed to the
//output.
void ShuntingYard::OperatorStack::popAll ()
{
	std::cout<<"Popping all"<<std::endl;

	while ( !m_Stack.empty() ) {
		PopToOutput();
	}

	std::cout<<"All have been popped"<<std::endl;
}

}

