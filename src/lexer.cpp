//STANDARD LIBRARY INCLUDES
#include <queue>   //std::queue
#include <regex>   //std::regex, std::regex_constants, std::smatch
#include <string>  //std::string
#include <utility> //std::pair

//INTERNAL INCLUDES
#include "lexer.hpp"    //ec::Lexer
#include "lexToken.hpp" //ec::LexToken

namespace ec {

Lexer::Lexer() :
	m_Status ( true ), //Nothing has failed yet, so status is true.
	m_LexTokenIdentifiers( { //A regex needs to be defined for every type of lexical token.
		std::pair<std::regex,LexToken::Type> ( //A function is anything between two underscores. This is because the preprocessor
			std::regex(R"(_.+?_)"),            //labels functions like this so that the lexer doesn't need to know their names.
			LexToken::Type::Function           //It's also because it's much easier to do that manually than with a regex.
		),
		std::pair<std::regex,LexToken::Type> ( //Binary operators are all checked manually, however as there are very few of them,
			std::regex(R"(\+|\-|\/|\*|\^)"),   //there's no need to get the preprocessor to help.
			LexToken::Type::BinaryOperator
		),
		std::pair<std::regex,LexToken::Type> ( //There is only one unary operator, although the general category exists in hope that
			std::regex(R"(~)"),                //there may be more in the future.
			LexToken::Type::UnaryOperator
		),
		std::pair<std::regex,LexToken::Type> ( //Separators are currently not actually used, however they would be if multivariate
			std::regex(R"(,)"),                //functions get added in. Until then, the lexer can easily identify them, and the
			LexToken::Type::Separator          //OperatorStack will throw an error when they are found.
		),
		std::pair<std::regex,LexToken::Type> ( //Numbers are defined as one or more digits, optionally followed by a decimal point
			std::regex(R"([0-9]+(\.[0-9]+)?)"),//and one or more digits. This ensure that 1. is not allowed, but 1.0 is.
			LexToken::Type::Number
		),
		std::pair<std::regex,LexToken::Type> ( //Simply '(' or ')'
			std::regex(R"(\(|\))"),
			LexToken::Type::Bracket
		),
		std::pair<std::regex,LexToken::Type> ( //A variable is any single letter that is not part of a function name.
			std::regex(R"([a-zA-Z])"),         //This does get rid of multiple character variable names, however it is necessary, as
			LexToken::Type::Variable           //if someone write "ax", they normally mean a*x, rather than the variable ax.
		)                                      //This is somewhat countered by the fact that the preprocessor can deal with
	} )                                        //predefined multiple character constants by replacing them with their value.
{
}

//Identify all the units which make up the expression and store this information in tokens.
std::queue<LexToken> Lexer::operator() ( std::string expression )
{
	m_Status = true;

	//A queue is used because it is easier for the shunting yard to have the tokens in the original order.
	std::queue<LexToken> outputQueue;

	//The process removes the lexed token from the front of the expression, so it is finished when there is no expression left.
	while ( expression.size() != 0 ) {
		bool identified (false);

		//The lex token identifiers are ordered such that no ambiguities occur. For example, Variable would match every letter in
		//each function name if it weren't for the fact that they will already have been removed because Function comes before it.
		for ( auto& x: m_LexTokenIdentifiers ) {
			std::smatch result; //There will only ever be one result, so this is more simple than it would otherwise be.

			//match_continuous simply means that matches must start at the start of the searched expression
			if ( std::regex_search ( expression, result, x.first, std::regex_constants::match_continuous ) ) {
				std::string lexeme = result.str();
				//The shunting yard and parser can do without having to deal with all the underscores, so they are removed.
				if ( x.second == LexToken::Type::Function ) {
					lexeme = lexeme.substr( 1, lexeme.length() - 2 );
				}

				//Add the discovered token to the output queue and remove it so that the next one can be found.
				outputQueue.emplace ( lexeme, x.second );
				expression.erase ( 0, result.str().length() );

				identified = true;
				break;
			}
		}

		if ( !identified ) {
			m_Status = false;
			return outputQueue;
		}
	}

	return outputQueue;
}

//In theory the lexer should never fail, as it should never recieve unexpected input and its function is quite simple, however this
//check still exists because in practice, bugs do happen.
bool Lexer::getStatus() const
{
	return m_Status;
}

}
