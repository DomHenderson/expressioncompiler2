//STANDARD LIBRARY INCLUDES
#include <iostream>      //std::cout, std::endl
#include <memory>        //std::make_unique, std::unique_ptr
#include <queue>         //std::queue
#include <stack>         //std::stack
#include <string>        //std::stod, std::string
#include <unordered_map> //std::unordered_map
#include <utility>       //std::move

//INTERNAL INCLUDES
#include "parser.hpp"        //ec::Parser
#include "parseNode.hpp"     //ec::ParseNode
#include "parseToken.hpp"    //ec::ParseToken
#include "shuntingToken.hpp" //ec::ShuntingToken

namespace ec {

Parser::Parser() :
	m_Status ( true )
{
}

ParseNode::Ptr Parser::operator() ( std::stack<ShuntingToken> tokens )
{
	//m_Status starts off as true and is set to false when an error token is found.
	m_Status = true;

	//ParseTokens must first be created before ParseNodes, as each types of ParseToken defines how to convert it into a ParseNode.
	//Defining this process via virtual functions makes it much more maintainable, extensible, and legible.
	//Pointers are used because the ParseTokens are used polymorphically.
	std::queue<std::unique_ptr<ParseToken>> parseTokens;

	//To further increase legibility, each conversion has its own dedicated function.
	//The special case is error tokens, which simply stop the process and return the fact that something has gone wrong.
	//In my experience, this is most likely to be mismatched brackets as anything else will probably have already been detected.
	//std::move is used because unique_ptrs cannot be copied, as they maintain unique ownership. However the ownership can be transferred.
	while ( !tokens.empty() ) {
		switch ( tokens.top().getType() ) {
		case ShuntingToken::Type::BinaryOperator:
			parseTokens.push ( std::move ( BinaryOperatorToToken ( tokens.top().getLexeme() ) ) );
			break;

		case ShuntingToken::Type::Function:
			parseTokens.push ( std::move ( FunctionToToken ( tokens.top().getLexeme() ) ) );
			break;

		case ShuntingToken::Type::Number:
			parseTokens.push ( std::move ( NumberToToken ( tokens.top().getLexeme() ) ) );
			break;

		case ShuntingToken::Type::UnaryOperator:
			parseTokens.push ( std::move ( UnaryOperatorToToken ( tokens.top().getLexeme() ) ) );
			break;

		case ShuntingToken::Type::Variable:
			parseTokens.push ( std::move ( VariableToToken ( tokens.top().getLexeme() ) ) );
			break;

		case ShuntingToken::Type::Error:
			m_Status = false;
			std::cout<<"Error shunting token with lexeme "<<tokens.top().getLexeme()<<std::endl;
			return std::unique_ptr<ParseNode>();
			break;
		}
		tokens.pop();
	}

	//This is the only way to print the contents of a queue without losing them.
	std::queue<std::unique_ptr<ParseToken>> temp;
	while ( !parseTokens.empty() ) {
		parseTokens.front()->print();
		temp.push ( std::move ( parseTokens.front() ) );
		parseTokens.pop();
	}
	parseTokens.swap ( temp );

	//The first ParseToken must be separated from the rest, as the rest must be passed into the first's getNodePtr().
	std::unique_ptr<ParseToken> first = std::move ( parseTokens.front() );
	parseTokens.pop();

	//This recursively converts the entire queue into a single tree.
	std::unique_ptr<ParseNode> rootNode = std::move ( first->getNodePtr ( parseTokens ) );

	if ( !rootNode->isValid() ) {
		m_Status = false;
	}

	//Only the root node of the tree is needed as it owns the rest of the tree with smart pointers, and all functions are recursive.
	return std::move ( rootNode );
}

//This is very similar to the main function of Parser, but it is used very differently.
//This function is called by ParseNodes during the conversion from ParseTokens to ParseNodes, when custom functions need to be
//replacd with a tree of existing node types. Therefore, the definition of the function is parsed, and the resulting tree is returned.
ParseNode::Ptr Parser::getCustomFunctionTree ( std::string name, std::queue<std::unique_ptr<ParseToken>>& inputTokens )
{
	m_Status = true;

	std::queue<std::unique_ptr<ParseToken>> parseTokens;

	//Create the custom function's operand and remove those nodes from inputTokens
	std::unique_ptr<ParseToken> operand = std::move ( inputTokens.front() );
	inputTokens.pop();
	ParseNode::Ptr operandNode = std::move ( operand->getNodePtr ( inputTokens ) );

	std::stack<ShuntingToken> tokens = m_ReferenceFunctions[name].tokens;

	while ( !tokens.empty() ) {
		switch ( tokens.top().getType() ) {
			case ShuntingToken::Type::BinaryOperator:
				parseTokens.push ( std::move ( BinaryOperatorToToken ( tokens.top().getLexeme() ) ) );
				break;

			case ShuntingToken::Type::Function:
				parseTokens.push ( std::move ( FunctionToToken ( tokens.top().getLexeme() ) ) );
				break;

			case ShuntingToken::Type::Number:
				parseTokens.push ( std::move ( NumberToToken ( tokens.top().getLexeme() ) ) );
				break;

			case ShuntingToken::Type::UnaryOperator:
				parseTokens.push ( std::move ( UnaryOperatorToToken ( tokens.top().getLexeme() ) ) );
				break;

			case ShuntingToken::Type::Variable:
				//The independent variable of the function needs to be replaced by the expression passed to the function.
				//e.g. in f : x -> x^2, f : sin(x) -> (sin(x))^2
				//All other variables that may be present in the function are extraneous and act like normal variables in the outer function.
				if ( tokens.top().getLexeme() == m_ReferenceFunctions[name].variable ) {
					parseTokens.push ( std::make_unique<CustomFunctionInputParseToken> ( operandNode->getCopy() ) );
				} else {
					parseTokens.push ( std::move ( VariableToToken ( tokens.top().getLexeme() ) ) );
				}
				break;

			case ShuntingToken::Type::Error:
				m_Status = false;
				std::cout<<"Error shunting token with lexeme "<<tokens.top().getLexeme()<<std::endl;
				return std::unique_ptr<ParseNode>();
				break;
		}
		tokens.pop();
	}

	std::queue<std::unique_ptr<ParseToken>> temp;
	while ( !parseTokens.empty() ) {
		parseTokens.front()->print();
		temp.push ( std::move ( parseTokens.front() ) );
		parseTokens.pop();
	}
	parseTokens.swap ( temp );

	std::unique_ptr<ParseToken> first = std::move ( parseTokens.front() );
	parseTokens.pop();
	std::unique_ptr<ParseNode> rootNode = std::move ( first->getNodePtr ( parseTokens ) );

	return std::move ( rootNode );
}

//Add or remove functions from the stored list of user defined functions
void Parser::addFunction ( std::string name, std::string variable, std::stack<ShuntingToken> tokens )
{
	m_ReferenceFunctions[name] = { variable, tokens };
}

void Parser::removeFunction ( std::string name )
{
	m_ReferenceFunctions.erase ( name );
}

bool Parser::getStatus() const
{
	return m_Status;
}

//The following are manual conversions that are required because of the fact that ParseTokens are based of enums that are much more
//specific than ShuntingTokens. Almost all ParseTokens do not actually have a lexeme because the enums cover every possibility.
std::unique_ptr<ParseToken> Parser::BinaryOperatorToToken ( std::string lexeme )
{
	if ( lexeme == "+" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Addition );
	} else if ( lexeme == "-" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Subtraction );
	} else if ( lexeme == "*" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Multiplication );
	} else if ( lexeme == "/" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Division );
	} else if ( lexeme == "^" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Power );
	} else {
		m_Status = false;
		return std::unique_ptr<OperationParseToken>();
	}
}

std::unique_ptr<ParseToken> Parser::FunctionToToken( std::string lexeme )
{
	if ( lexeme == "acosech" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Acosech );
	} else if ( lexeme == "acosec" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Acosec );
	} else if ( lexeme == "cosech" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Cosech );
	} else if ( lexeme == "acosh" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Acosh );
	} else if ( lexeme == "acoth" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Acoth );
	} else if ( lexeme == "asech" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Asech );
	} else if ( lexeme == "asinh" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Asinh );
	} else if ( lexeme == "atanh" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Atanh );
	} else if ( lexeme == "cosec" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Cosec );
	} else if ( lexeme == "acos" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Acos );
	} else if ( lexeme == "acot" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Acot );
	} else if ( lexeme == "asec" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Asec );
	} else if ( lexeme == "asin" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Asin );
	} else if ( lexeme == "atan" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Atan );
	} else if ( lexeme == "cosh" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Cosh );
	} else if ( lexeme == "coth" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Coth );
	} else if ( lexeme == "log2" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Log2 );
	} else if ( lexeme == "sech" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Sech );
	} else if ( lexeme == "sinh" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Sinh );
	} else if ( lexeme == "tanh" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Tanh );
	} else if ( lexeme == "abs" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Abs );
	} else if ( lexeme == "cos" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Cos );
	} else if ( lexeme == "cot" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Cot );
	} else if ( lexeme == "exp" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Exp );
	} else if ( lexeme == "log" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Log );
	} else if ( lexeme == "sec" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Sec );
	} else if ( lexeme == "sin" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Sin );
	} else if ( lexeme == "tan" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Tan );
	} else if ( lexeme == "ln" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Ln );
	} else if ( m_ReferenceFunctions.count ( lexeme ) ) { //Count can only return 1 or 0 so can be used as a bool
		return std::make_unique<CustomFunctionParseToken> ( lexeme, *this );
	} else {
		m_Status = false;
		return std::unique_ptr<OperationParseToken>();
	}
}

std::unique_ptr<ParseToken> Parser::NumberToToken( std::string lexeme )
{
	return std::make_unique<ConstantParseToken> ( std::stod ( lexeme ) );
}

std::unique_ptr<ParseToken> Parser::UnaryOperatorToToken ( std::string lexeme )
{
	if ( lexeme == "~" ) {
		return std::make_unique<OperationParseToken> ( OperationParseToken::Type::Negation );
	} else {
		m_Status = false;
		return std::unique_ptr<OperationParseToken>();
	}
}

std::unique_ptr<ParseToken> Parser::VariableToToken ( std::string lexeme )
{
	return std::make_unique<ParameterParseToken> ( lexeme );
}

}
