//STANDARD LIBRARY INCLUDES
#include <cmath>         //*lots* std::sin, std::cos etc
#include <functional>    //std::functional
#include <memory>        //std::make_unique
#include <queue>         //std::queue
#include <string>        //std::string, std::to_string
#include <unordered_map> //std::unordered_map
#include <utility>       //std::move

//INTERNAL INCLUDES
#include "parseNode.hpp"  //ec::ParseNode etc
#include "parseToken.hpp" //ec::ParseToken

namespace ec {

//Switches trig functions between taking their input in degrees, radians, or gradians.
void ParseNode::setDRGMode ( DRG mode )
{
	//DRGConversion is a multiplier to convert the chosen unit to radians.
	//This multiplier is used internally by all the trig function ParseNodes such that extra nodes are not needed and the expression
	//remains simple and legible to the user.
	switch ( mode ) {
	case DRG::Degrees:
		DRGConversion = 0.01745329252; //There are Pi/180 radians in a degree.
		break;

	case DRG::Radians:
		DRGConversion = 1.0; //There is one radian in a radian.
		break;

	case DRG::Gradians:
		DRGConversion = 0.015707963267949; //There are Pi/200 radians in a gradian.
		break;
	};
}

//The default mode is radians.
double ParseNode::DRGConversion = 1.0;

//The following is the definition of every type of ParseNode that could be needed. This is VERY long winded, but much simpler than
//making them based on maps and templates, which would simply be harder to read, and quite possibly actually harder to maintain.
//The actual functions themselves are quite self explanitory if you understand recursive functions in trees, there's just a lot of them.

ParameterParseNode::ParameterParseNode ( std::string name ):
	m_Name ( name )
{
}

ParseNode::Ptr ParameterParseNode::getCopy()
{
	return std::make_unique<ParameterParseNode> ( m_Name );
}

ParseNode::Ptr ParameterParseNode::getDifferential( std::string variable )
{
	if ( variable == m_Name ) {
		return std::make_unique<ConstantParseNode>( 1.0 );
	} else {
		return std::make_unique<ConstantParseNode>( 0.0 );
	}
}

std::function<double(std::unordered_map<std::string,double>&)> ParameterParseNode::getFunction()
{
	std::string name = m_Name;

	return [ name ] ( std::unordered_map<std::string, double>& parameters ) -> double {
		return parameters [ name ];
	};
}

std::string ParameterParseNode::getName() const
{
	return m_Name;
}

ParseNode::Ptr ParameterParseNode::simplify()
{
	return getCopy();
}

ConstantParseNode::ConstantParseNode ( double value ):
	m_Value ( value )
{
}

ParseNode::Ptr ConstantParseNode::getCopy()
{
	return std::make_unique<ConstantParseNode> ( m_Value );
}

ParseNode::Ptr ConstantParseNode::getDifferential ( std::string ) //Unnamed parameters can't be used, so gets rid of warnings
{
	return std::make_unique<ConstantParseNode>(0);
}

std::function<double(std::unordered_map<std::string,double>&)> ConstantParseNode::getFunction()
{
	double value = m_Value;

	return [ value ] ( std::unordered_map<std::string, double>& ) -> double { //Same technique as above
		return value;
	};
}

std::string ConstantParseNode::getName() const
{
	std::string name = std::to_string ( m_Value );

	while ( name [ name.length() - 1 ] == '0' ) {
		name.pop_back();
		if ( name [ name.length() - 1 ] == '.' ) {
			name.pop_back();
			break;
		}
	}

	return name;
}

ParseNode::Ptr ConstantParseNode::simplify()
{
	return getCopy();
}

AdditionParseNode::AdditionParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto rightToken = std::move ( tokens.front() );
		tokens.pop();
		m_Right = rightToken->getNodePtr ( tokens );
	} else {
		m_Right = std::unique_ptr<ParseNode>();
	}

	if ( !tokens.empty() ) {
		auto leftToken = std::move ( tokens.front() );
		tokens.pop();
		m_Left = leftToken->getNodePtr ( tokens );
	} else {
		m_Left = std::unique_ptr<ParseNode>();
	}
}

AdditionParseNode::AdditionParseNode ( ParseNode::Ptr left, ParseNode::Ptr right ) :
	m_Left ( std::move ( left ) ), m_Right ( std::move ( right ) )
{
}

ParseNode::Ptr AdditionParseNode::getCopy()
{
	return std::make_unique<AdditionParseNode> ( m_Left->getCopy(), m_Right->getCopy() );
}

ParseNode::Ptr AdditionParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<AdditionParseNode> ( m_Left->getDifferential( variable ), m_Right->getDifferential( variable ) );
}

std::function<double(std::unordered_map<std::string,double>&)> AdditionParseNode::getFunction()
{
	std::function<double(std::unordered_map<std::string,double>&)> leftFunc = m_Left->getFunction();
	std::function<double(std::unordered_map<std::string,double>&)> rightFunc = m_Right->getFunction();
	return [ leftFunc, rightFunc ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return leftFunc ( parameters ) + rightFunc ( parameters );
	};
}

std::string AdditionParseNode::getName() const
{
	std::string left;
	std::string right;

	if ( m_Left->getPriority() < getPriority() ) {
		left = "(" + m_Left->getName() + ")";
	} else {
		left = m_Left->getName();
	}

	if ( m_Right->getPriority() <= getPriority() ) {
		right = "(" + m_Right->getName() + ")";
	} else {
		right = m_Right->getName();
	}

	return left + "+" + right;
}

ParseNode::Ptr AdditionParseNode::simplify()
{
	m_Left = m_Left->simplify();
	m_Right = m_Right->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Left->isConstant() && m_Right->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else if ( m_Left->isConstant() && m_Left->getFunction() ( parameters ) == 0.0 ) {
		return m_Right->getCopy();
	} else if ( m_Right->isConstant() && m_Right->getFunction() ( parameters ) == 0.0 ) {
		return m_Left->getCopy();
	} else {
		return getCopy();
	}
}

SubtractionParseNode::SubtractionParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto rightToken = std::move ( tokens.front() );
		tokens.pop();
		m_Right = rightToken->getNodePtr ( tokens );
	} else {
		m_Right = std::unique_ptr<ParseNode>();
	}

	if ( !tokens.empty() ) {
		auto leftToken = std::move ( tokens.front() );
		tokens.pop();
		m_Left = leftToken->getNodePtr ( tokens );
	} else {
		m_Left = std::unique_ptr<ParseNode>();
	}
}

SubtractionParseNode::SubtractionParseNode ( ParseNode::Ptr left, ParseNode::Ptr right ) :
	m_Left ( std::move ( left ) ), m_Right ( std::move ( right ) )
{
}

ParseNode::Ptr SubtractionParseNode::getCopy()
{
	return std::make_unique<SubtractionParseNode> ( m_Left->getCopy(), m_Right->getCopy() );
}

ParseNode::Ptr SubtractionParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<SubtractionParseNode> ( m_Left->getDifferential ( variable ), m_Right->getDifferential ( variable ) );
}

std::function<double(std::unordered_map<std::string,double>&)> SubtractionParseNode::getFunction()
{
	auto leftFunc = m_Left->getFunction();
	auto rightFunc = m_Right->getFunction();
	return [ leftFunc, rightFunc ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return leftFunc ( parameters ) - rightFunc ( parameters );
	};
}

std::string SubtractionParseNode::getName() const
{
	std::string left;
	std::string right;

	if ( m_Left->getPriority() < getPriority() ) {
		left = "(" + m_Left->getName() + ")";
	} else {
		left = m_Left->getName();
	}

	if ( m_Right->getPriority() <= getPriority() ) {
		right = "(" + m_Right->getName() + ")";
	} else {
		right = m_Right->getName();
	}

	return left + "-" + right;
}

ParseNode::Ptr SubtractionParseNode::simplify()
{
	m_Left = m_Left->simplify();
	m_Right = m_Right->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Left->isConstant() && m_Right->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else if ( m_Left->isConstant() && m_Left->getFunction() ( parameters ) == 0.0 ) {
		return std::make_unique<NegationParseNode> ( m_Right->getCopy() );
	} else if ( m_Right->isConstant() && m_Right->getFunction() ( parameters ) == 0.0 ) {
		return m_Left->getCopy();
	} else {
		return getCopy();
	}
}

MultiplicationParseNode::MultiplicationParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto rightToken = std::move ( tokens.front() );
		tokens.pop();
		m_Right = rightToken->getNodePtr ( tokens );
	} else {
		m_Right = std::unique_ptr<ParseNode>();
	}

	if ( !tokens.empty() ) {
		auto leftToken = std::move ( tokens.front() );
		tokens.pop();
		m_Left = leftToken->getNodePtr ( tokens );
	} else {
		m_Left = std::unique_ptr<ParseNode>();
	}
}

MultiplicationParseNode::MultiplicationParseNode ( ParseNode::Ptr left, ParseNode::Ptr right ) :
	m_Left ( std::move ( left ) ), m_Right ( std::move ( right ) )
{

}

ParseNode::Ptr MultiplicationParseNode::getCopy()
{
	return std::make_unique<MultiplicationParseNode> ( m_Left->getCopy(), m_Right->getCopy() );
}

ParseNode::Ptr MultiplicationParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<AdditionParseNode> (
		std::make_unique<MultiplicationParseNode> (
			m_Left->getDifferential ( variable ),
			m_Right->getCopy()
		),
		std::make_unique<MultiplicationParseNode> (
			m_Left->getCopy(),
			m_Right->getDifferential ( variable )
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> MultiplicationParseNode::getFunction()
{
	auto leftFunc = m_Left->getFunction();
	auto rightFunc = m_Right->getFunction();
	return [ leftFunc, rightFunc ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return leftFunc ( parameters ) * rightFunc ( parameters );
	};
}

std::string MultiplicationParseNode::getName() const
{
	std::string left;
	std::string right;

	if ( m_Left->getPriority() < getPriority() ) {
		left = "(" + m_Left->getName() + ")";
	} else {
		left = m_Left->getName();
	}

	if ( m_Right->getPriority() <= getPriority() ) {
		right = "(" + m_Right->getName() + ")";
	} else {
		right = m_Right->getName();
	}

	return left + "*" + right;
}

ParseNode::Ptr MultiplicationParseNode::simplify()
{
	m_Left = m_Left->simplify();
	m_Right = m_Right->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Left->isConstant() && m_Right->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else if ( m_Left->isConstant() && m_Left->getFunction() ( parameters ) == 0.0 ) {
		return std::make_unique<ConstantParseNode> ( 0.0 );
	} else if ( m_Right->isConstant() && m_Right->getFunction() ( parameters ) == 0.0 ) {
		return std::make_unique<ConstantParseNode> ( 0.0 );
	} else if ( m_Left->isConstant() && m_Left->getFunction() ( parameters ) == 1.0 ) {
		return m_Right->getCopy();
	} else if ( m_Right->isConstant() && m_Right->getFunction() ( parameters ) == 1.0 ) {
		return m_Left->getCopy();
	} else {
		return getCopy();
	}
}

DivisionParseNode::DivisionParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto rightToken = std::move ( tokens.front() );
		tokens.pop();
		m_Right = rightToken->getNodePtr ( tokens );
	} else {
		m_Right = std::unique_ptr<ParseNode>();
	}

	if ( !tokens.empty() ) {
		auto leftToken = std::move ( tokens.front() );
		tokens.pop();
		m_Left = leftToken->getNodePtr ( tokens );
	} else {
		m_Left = std::unique_ptr<ParseNode>();
	}
}

DivisionParseNode::DivisionParseNode ( ParseNode::Ptr left, ParseNode::Ptr right ) :
	m_Left ( std::move ( left ) ), m_Right ( std::move ( right ) )
{
}

ParseNode::Ptr DivisionParseNode::getCopy()
{
	return std::make_unique<DivisionParseNode> ( m_Left->getCopy(), m_Right->getCopy() );
}

ParseNode::Ptr DivisionParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		std::make_unique<SubtractionParseNode> (
			std::make_unique<MultiplicationParseNode> (
				m_Right->getCopy(),
				m_Left->getDifferential ( variable )
			),
			std::make_unique<MultiplicationParseNode> (
				m_Left->getCopy(),
				m_Right->getDifferential ( variable )
			)
		),
		std::make_unique<PowerParseNode> (
			m_Right->getCopy(),
			std::make_unique<ConstantParseNode> ( 2 )
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> DivisionParseNode::getFunction()
{
	auto leftFunc = m_Left->getFunction();
	auto rightFunc = m_Right->getFunction();
	return [ leftFunc, rightFunc ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return leftFunc ( parameters ) / rightFunc ( parameters );
	};
}

std::string DivisionParseNode::getName() const
{
	std::string left;
	std::string right;

	if ( m_Left->getPriority() < getPriority() ) {
		left = "(" + m_Left->getName() + ")";
	} else {
		left = m_Left->getName();
	}

	if ( m_Right->getPriority() <= getPriority() ) {
		right = "(" + m_Right->getName() + ")";
	} else {
		right = m_Right->getName();
	}

	return left + "/" + right;
}

ParseNode::Ptr DivisionParseNode::simplify()
{
	m_Left = m_Left->simplify();
	m_Right = m_Right->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Left->isConstant() && m_Right->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else if ( m_Left->isConstant() && m_Left->getFunction() ( parameters ) == 0.0 ) {
		return std::make_unique<ConstantParseNode> ( 0.0 );
	} else if ( m_Right->isConstant() && m_Right->getFunction() ( parameters ) == 1.0 ) {
		return m_Left->getCopy();
	} else {
		return getCopy();
	}
}

PowerParseNode::PowerParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto rightToken = std::move ( tokens.front() );
		tokens.pop();
		m_Right = rightToken->getNodePtr ( tokens );
	} else {
		m_Right = std::unique_ptr<ParseNode>();
	}

	if ( !tokens.empty() ) {
		auto leftToken = std::move ( tokens.front() );
		tokens.pop();
		m_Left = leftToken->getNodePtr ( tokens );
	} else {
		m_Left = std::unique_ptr<ParseNode>();
	}
}

PowerParseNode::PowerParseNode ( ParseNode::Ptr left, ParseNode::Ptr right ) :
	m_Left ( std::move ( left ) ), m_Right ( std::move ( right ) )
{
}

ParseNode::Ptr PowerParseNode::getCopy()
{
	return std::make_unique<PowerParseNode> ( m_Left->getCopy(), m_Right->getCopy() );
}

ParseNode::Ptr PowerParseNode::getDifferential( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		std::make_unique<AdditionParseNode> (
			std::make_unique<MultiplicationParseNode> (
				m_Right->getCopy(),
				m_Left->getDifferential( variable )
			),
			std::make_unique<MultiplicationParseNode> (
				std::make_unique<MultiplicationParseNode> (
					m_Left->getCopy(),
					std::make_unique<LnParseNode> (
						m_Left->getCopy()
					)
				),
				m_Right->getDifferential( variable )
			)
		),
		std::make_unique<PowerParseNode> (
			m_Left->getCopy(),
			std::make_unique<SubtractionParseNode> (
				m_Right->getCopy(),
				std::make_unique<ConstantParseNode> ( 1.0 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> PowerParseNode::getFunction()
{
	auto leftFunc = m_Left->getFunction();
	auto rightFunc = m_Right->getFunction();
	return [ leftFunc, rightFunc ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::pow ( leftFunc ( parameters ), rightFunc ( parameters ) );
	};
}

std::string PowerParseNode::getName() const
{
	std::string left;
	std::string right;

	if ( m_Left->getPriority() <= getPriority() ) {
		left = "(" + m_Left->getName() + ")";
	} else {
		left = m_Left->getName();
	}

	if ( m_Right->getPriority() < getPriority() ) {
		right = "(" + m_Right->getName() + ")";
	} else {
		right = m_Right->getName();
	}

	return left + "^" + right;
}

ParseNode::Ptr PowerParseNode::simplify()
{
	m_Left = m_Left->simplify();
	m_Right = m_Right->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Left->isConstant() && m_Right->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else if ( m_Right->isConstant() && m_Right->getFunction() ( parameters ) == 0.0 ) {
		return std::make_unique<ConstantParseNode> ( 1.0 );
	} else if ( m_Left->isConstant() && m_Left->getFunction() ( parameters ) == 1.0 ) {
		return std::make_unique<ConstantParseNode> ( 1.0 );
	} else if ( m_Right->isConstant() && m_Right->getFunction() ( parameters ) == 1.0 ) {
		return m_Left->getCopy();
	} else {
		return getCopy();
	}
}

NegationParseNode::NegationParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

NegationParseNode::NegationParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr NegationParseNode::getCopy()
{
	return std::make_unique<NegationParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr NegationParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<NegationParseNode> ( m_Operand->getDifferential ( variable ) );
}

std::function<double(std::unordered_map<std::string,double>&)> NegationParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return -operand ( parameters );
	};
}

ParseNode::Ptr NegationParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string NegationParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "-(" + m_Operand->getName() + ")";
	} else {
		return "-" + m_Operand->getName();
	}
}

AcosechParseNode::AcosechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AcosechParseNode::AcosechParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AcosechParseNode::getCopy()
{
	return std::make_unique<AcosechParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AcosechParseNode::getDifferential( std::string variable )
{
	return std::make_unique<NegationParseNode> (
		std::make_unique<DivisionParseNode> (
			m_Operand->getDifferential ( variable ),
			std::make_unique<MultiplicationParseNode> (
				std::make_unique<PowerParseNode> (
					std::make_unique<AdditionParseNode> (
						std::make_unique<DivisionParseNode> (
							std::make_unique<ConstantParseNode> ( 1 ),
							std::make_unique<PowerParseNode> (
								m_Operand->getCopy(),
								std::make_unique<ConstantParseNode> ( 2 )
							)
						),
						std::make_unique<ConstantParseNode> ( 1 )
					),
					std::make_unique<ConstantParseNode> ( 0.5 )
				),
				std::make_unique<PowerParseNode> (
					m_Operand->getCopy(),
					std::make_unique<ConstantParseNode> ( 2 )
				)
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AcosechParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::asinh ( 1 / operand ( parameters ) );
	};
}

ParseNode::Ptr AcosechParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AcosechParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "acosech(" + m_Operand->getName() + ")";
	} else {
		return "acosech" + m_Operand->getName();
	}
}

AcosecParseNode::AcosecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AcosecParseNode::AcosecParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AcosecParseNode::getCopy()
{
	return std::make_unique<AcosecParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AcosecParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<NegationParseNode> (
		std::make_unique<DivisionParseNode> (
			m_Operand->getDifferential ( variable ),
			std::make_unique<MultiplicationParseNode> (
				std::make_unique<PowerParseNode> (
					std::make_unique<SubtractionParseNode> (
						std::make_unique<ConstantParseNode> ( 1 ),
						std::make_unique<DivisionParseNode> (
							std::make_unique<ConstantParseNode> ( 1 ),
							std::make_unique<PowerParseNode> (
								m_Operand->getCopy(),
								std::make_unique<ConstantParseNode> ( 2 )
							)
						)
					),
					std::make_unique<ConstantParseNode> ( 0.5 )
				),
				std::make_unique<PowerParseNode> (
					m_Operand->getCopy(),
					std::make_unique<ConstantParseNode> ( 2 )
				)
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AcosecParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::asin ( 1 / operand ( parameters ) )/ParseNode::DRGConversion;
	};
}

ParseNode::Ptr AcosecParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AcosecParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "acosec(" + m_Operand->getName() + ")";
	} else {
		return "acosec" + m_Operand->getName();
	}
}

CosechParseNode::CosechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

CosechParseNode::CosechParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr CosechParseNode::getCopy()
{
	return std::make_unique<CosechParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr CosechParseNode::getDifferential( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		std::make_unique<MultiplicationParseNode> (
			m_Operand->getDifferential( variable ),
			std::make_unique<NegationParseNode> (
				std::make_unique<CothParseNode> (
					m_Operand->getCopy()
				)
			)
		),
		std::make_unique<CosechParseNode> (
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> CosechParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return 1 / std::sinh ( operand ( parameters ) );
	};
}

ParseNode::Ptr CosechParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string CosechParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "cosech(" + m_Operand->getName() + ")";
	} else {
		return "cosech" + m_Operand->getName();
	}
}

AcoshParseNode::AcoshParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AcoshParseNode::AcoshParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AcoshParseNode::getCopy()
{
	return std::make_unique<AcoshParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AcoshParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<MultiplicationParseNode> (
			std::make_unique<PowerParseNode> (
				std::make_unique<SubtractionParseNode> (
					m_Operand->getCopy(),
					std::make_unique<ConstantParseNode> ( 1 )
				),
				std::make_unique<ConstantParseNode> ( 0.5 )
			),
			std::make_unique<PowerParseNode> (
				std::make_unique<AdditionParseNode> (
					m_Operand->getCopy(),
					std::make_unique<ConstantParseNode> ( 1 )
				),
				std::make_unique<ConstantParseNode> ( 0.5 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AcoshParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::acosh ( operand ( parameters ) );
	};
}

ParseNode::Ptr AcoshParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AcoshParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "acosh(" + m_Operand->getName() + ")";
	} else {
		return "acosh" + m_Operand->getName();
	}
}

AcothParseNode::AcothParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AcothParseNode::AcothParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AcothParseNode::getCopy()
{
	return std::make_unique<AcothParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AcothParseNode::getDifferential( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<SubtractionParseNode> (
			std::make_unique<ConstantParseNode> ( 1 ),
			std::make_unique<PowerParseNode> (
				m_Operand->getCopy(),
				std::make_unique<ConstantParseNode> ( 2 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AcothParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::atanh ( 1 / operand ( parameters ) );
	};
}

ParseNode::Ptr AcothParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AcothParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "acoth(" + m_Operand->getName() + ")";
	} else {
		return "acoth" + m_Operand->getName();
	}
}

AsechParseNode::AsechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AsechParseNode::AsechParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AsechParseNode::getCopy()
{
	return std::make_unique<AsechParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AsechParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		std::make_unique<MultiplicationParseNode> (
			std::make_unique<PowerParseNode> (
				std::make_unique<DivisionParseNode> (
					std::make_unique<SubtractionParseNode> (
						std::make_unique<ConstantParseNode> ( 1 ),
						m_Operand->getCopy()
					),
					std::make_unique<AdditionParseNode> (
						m_Operand->getCopy(),
						std::make_unique<ConstantParseNode> ( 1 )
					)
				),
				std::make_unique<ConstantParseNode> ( 0.5 )
			),
			m_Operand->getDifferential( variable )
		),
		std::make_unique<MultiplicationParseNode> (
			std::make_unique<SubtractionParseNode> (
				m_Operand->getCopy(),
				std::make_unique<ConstantParseNode> ( 1 )
			),
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AsechParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::acosh ( 1 / operand ( parameters ) );
	};
}

ParseNode::Ptr AsechParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AsechParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "asech(" + m_Operand->getName() + ")";
	} else {
		return "asech" + m_Operand->getName();
	}
}

AsinhParseNode::AsinhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AsinhParseNode::AsinhParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AsinhParseNode::getCopy()
{
	return std::make_unique<AsinhParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AsinhParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential( variable ),
		std::make_unique<PowerParseNode> (
			std::make_unique<AdditionParseNode> (
				std::make_unique<PowerParseNode> (
					m_Operand->getCopy(),
					std::make_unique<ConstantParseNode> ( 2 )
				),
				std::make_unique<ConstantParseNode> ( 1 )
			),
			std::make_unique<ConstantParseNode> ( 0.5 )
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AsinhParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::asinh ( operand ( parameters ) );
	};
}

ParseNode::Ptr AsinhParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AsinhParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "asinh(" + m_Operand->getName() + ")";
	} else {
		return "asinh" + m_Operand->getName();
	}
}

AtanhParseNode::AtanhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AtanhParseNode::AtanhParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AtanhParseNode::getCopy ()
{
	return std::make_unique<AtanhParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AtanhParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<SubtractionParseNode> (
			std::make_unique<ConstantParseNode> ( 1 ),
			std::make_unique<PowerParseNode> (
				m_Operand->getCopy(),
				std::make_unique<ConstantParseNode> ( 2 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AtanhParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::atanh ( operand ( parameters ) );
	};
}

ParseNode::Ptr AtanhParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AtanhParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "atanh(" + m_Operand->getName() + ")";
	} else {
		return "atanh" + m_Operand->getName();
	}
}

CosecParseNode::CosecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

CosecParseNode::CosecParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr CosecParseNode::getCopy()
{
	return std::make_unique<CosecParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr CosecParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		std::make_unique<MultiplicationParseNode> (
			m_Operand->getDifferential( variable ),
			std::make_unique<NegationParseNode> (
				std::make_unique<CotParseNode> (
					m_Operand->getCopy()
				)
			)
		),
		std::make_unique<CosecParseNode> (
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> CosecParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return 1 / std::sin ( operand ( parameters ) * ParseNode::DRGConversion );
	};
}

ParseNode::Ptr CosecParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string CosecParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "cosec(" + m_Operand->getName() + ")";
	} else {
		return "cosec" + m_Operand->getName();
	}
}

AcosParseNode::AcosParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AcosParseNode::AcosParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AcosParseNode::getCopy()
{
	return std::make_unique<AcosParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AcosParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<NegationParseNode> (
		std::make_unique<DivisionParseNode> (
			m_Operand->getDifferential ( variable ),
			std::make_unique<PowerParseNode> (
				std::make_unique<SubtractionParseNode> (
					std::make_unique<ConstantParseNode> ( 1 ),
					std::make_unique<PowerParseNode> (
						m_Operand->getCopy(),
						std::make_unique<ConstantParseNode> ( 2 )
					)
				),
				std::make_unique<ConstantParseNode> ( 0.5 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AcosParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::acos ( operand ( parameters ) )/ParseNode::DRGConversion;
	};
}

ParseNode::Ptr AcosParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AcosParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "acos(" + m_Operand->getName() + ")";
	} else {
		return "acos" + m_Operand->getName();
	}
}

AcotParseNode::AcotParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AcotParseNode::AcotParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AcotParseNode::getCopy()
{
	return std::make_unique<AcotParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AcotParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<NegationParseNode> (
		std::make_unique<DivisionParseNode> (
			m_Operand->getDifferential ( variable ),
			std::make_unique<AdditionParseNode> (
				std::make_unique<PowerParseNode> (
					m_Operand->getCopy(),
					std::make_unique<ConstantParseNode> ( 2 )
				),
				std::make_unique<ConstantParseNode> ( 1 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AcotParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::atan ( 1 / operand ( parameters ) )/ParseNode::DRGConversion;
	};
}

ParseNode::Ptr AcotParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AcotParseNode::getName() const
{
if ( m_Operand->getPriority() < getPriority() ) {
		return "acot(" + m_Operand->getName() + ")";
	} else {
		return "acot" + m_Operand->getName();
	}
}

AsecParseNode::AsecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AsecParseNode::AsecParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AsecParseNode::getCopy()
{
	return std::make_unique<AsecParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AsecParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<MultiplicationParseNode> (
			std::make_unique<PowerParseNode> (
				std::make_unique<SubtractionParseNode> (
					std::make_unique<ConstantParseNode> ( 1 ),
					std::make_unique<DivisionParseNode> (
						std::make_unique<ConstantParseNode> ( 1 ),
						std::make_unique<PowerParseNode> (
							m_Operand->getCopy(),
							std::make_unique<ConstantParseNode> ( 2 )
						)
					)
				),
				std::make_unique<ConstantParseNode> ( 0.5 )
			),
			std::make_unique<PowerParseNode> (
				m_Operand->getCopy(),
				std::make_unique<ConstantParseNode> ( 2 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AsecParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::acos ( 1 / operand ( parameters ) )/ParseNode::DRGConversion;
	};
}

ParseNode::Ptr AsecParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AsecParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "asec(" + m_Operand->getName() + ")";
	} else {
		return "asec" + m_Operand->getName();
	}
}

AsinParseNode::AsinParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AsinParseNode::AsinParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AsinParseNode::getCopy()
{
	return std::make_unique<AsinParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AsinParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<PowerParseNode> (
			std::make_unique<SubtractionParseNode> (
				std::make_unique<ConstantParseNode> ( 1 ),
				std::make_unique<PowerParseNode> (
					m_Operand->getCopy(),
					std::make_unique<ConstantParseNode> ( 2 )
				)
			),
			std::make_unique<ConstantParseNode> ( 0.5 )
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AsinParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::asin ( operand ( parameters ) )/ParseNode::DRGConversion;
	};
}

ParseNode::Ptr AsinParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AsinParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "asin(" + m_Operand->getName() + ")";
	} else {
		return "asin" + m_Operand->getName();
	}
}

AtanParseNode::AtanParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AtanParseNode::AtanParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AtanParseNode::getCopy()
{
	return std::make_unique<AtanParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AtanParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<AdditionParseNode> (
			std::make_unique<PowerParseNode> (
				m_Operand->getCopy(),
				std::make_unique<ConstantParseNode> ( 2 )
			),
			std::make_unique<ConstantParseNode> ( 1 )
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AtanParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::atan ( operand ( parameters ) )/ParseNode::DRGConversion;
	};
}

ParseNode::Ptr AtanParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AtanParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "atan(" + m_Operand->getName() + ")";
	} else {
		return "atan" + m_Operand->getName();
	}
}

CoshParseNode::CoshParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

CoshParseNode::CoshParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr CoshParseNode::getCopy()
{
	return std::make_unique<CoshParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr CoshParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<SinhParseNode> (
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> CoshParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::cosh ( operand ( parameters ) );
	};
}

ParseNode::Ptr CoshParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string CoshParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "cosh(" + m_Operand->getName() + ")";
	} else {
		return "cosh" + m_Operand->getName();
	}
}

CothParseNode::CothParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

CothParseNode::CothParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr CothParseNode::getCopy()
{
	return std::make_unique<CothParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr CothParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<NegationParseNode> (
			std::make_unique<PowerParseNode> (
				std::make_unique<CosechParseNode> (
					m_Operand->getCopy()
				),
				std::make_unique<ConstantParseNode> ( 2 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> CothParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return 1 / std::tanh ( operand ( parameters ) );
	};
}

ParseNode::Ptr CothParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string CothParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "coth(" + m_Operand->getName() + ")";
	} else {
		return "coth" + m_Operand->getName();
	}
}

Log2ParseNode::Log2ParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

Log2ParseNode::Log2ParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr Log2ParseNode::getCopy()
{
	return std::make_unique<Log2ParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr Log2ParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<MultiplicationParseNode> (
			std::make_unique<LnParseNode> (
				std::make_unique<ConstantParseNode> ( 2 )
			),
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> Log2ParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::log2 ( operand ( parameters ) );
	};
}

ParseNode::Ptr Log2ParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string Log2ParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "log2(" + m_Operand->getName() + ")";
	} else {
		return "log2" + m_Operand->getName();
	}
}

SechParseNode::SechParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

SechParseNode::SechParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr SechParseNode::getCopy()
{
	return std::make_unique<SechParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr SechParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		std::make_unique<MultiplicationParseNode> (
			m_Operand->getDifferential ( variable ),
			std::make_unique<TanhParseNode> (
				m_Operand->getCopy()
			)
		),
		std::make_unique<NegationParseNode> (
			std::make_unique<SechParseNode> (
				m_Operand->getCopy()
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> SechParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return 1 / std::cosh ( operand ( parameters ) );
	};
}

ParseNode::Ptr SechParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string SechParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "sech(" + m_Operand->getName() + ")";
	} else {
		return "sech" + m_Operand->getName();
	}
}

SinhParseNode::SinhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

SinhParseNode::SinhParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr SinhParseNode::getCopy()
{
	return std::make_unique<SinhParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr SinhParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<CoshParseNode> (
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> SinhParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::sinh ( operand ( parameters ) );
	};
}

ParseNode::Ptr SinhParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string SinhParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "sinh(" + m_Operand->getName() + ")";
	} else {
		return "sinh" + m_Operand->getName();
	}
}

TanhParseNode::TanhParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

TanhParseNode::TanhParseNode ( ParseNode::Ptr op ) :
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr TanhParseNode::getCopy()
{
	return std::make_unique<TanhParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr TanhParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<PowerParseNode> (
			std::make_unique<SechParseNode> (
				m_Operand->getCopy()
			),
			std::make_unique<ConstantParseNode> ( 2 )
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> TanhParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::tanh ( operand ( parameters ) );
	};
}

ParseNode::Ptr TanhParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string TanhParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "tanh(" + m_Operand->getName() + ")";
	} else {
		return "tanh" + m_Operand->getName();
	}
}

AbsParseNode::AbsParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

AbsParseNode::AbsParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr AbsParseNode::getCopy()
{
	return std::make_unique<AbsParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr AbsParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		std::make_unique<MultiplicationParseNode> (
			m_Operand->getCopy(),
			m_Operand->getDifferential ( variable )
		),
		std::make_unique<AbsParseNode> (
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> AbsParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::abs ( operand ( parameters ) );
	};
}

ParseNode::Ptr AbsParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string AbsParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "abs(" + m_Operand->getName() + ")";
	} else {
		return "abs" + m_Operand->getName();
	}
}

CosParseNode::CosParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

CosParseNode::CosParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr CosParseNode::getCopy()
{
	return std::make_unique<CosParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr CosParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<NegationParseNode> (
			std::make_unique<SinParseNode> (
				m_Operand->getCopy()
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> CosParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::cos ( operand ( parameters )*ParseNode::DRGConversion );
	};
}

ParseNode::Ptr CosParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string CosParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "cos(" + m_Operand->getName() + ")";
	} else {
		return "cos" + m_Operand->getName();
	}
}

CotParseNode::CotParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

CotParseNode::CotParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr CotParseNode::getCopy()
{
	return std::make_unique<CotParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr CotParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<NegationParseNode> (
			std::make_unique<PowerParseNode> (
				std::make_unique<CosecParseNode> (
					m_Operand->getCopy()
				),
				std::make_unique<ConstantParseNode> ( 2 )
			)
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> CotParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return 1 / std::tan ( operand ( parameters )*ParseNode::DRGConversion );
	};
}

ParseNode::Ptr CotParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string CotParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "cot(" + m_Operand->getName() + ")";
	} else {
		return "cot" + m_Operand->getName();
	}
}

ExpParseNode::ExpParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

ExpParseNode::ExpParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr ExpParseNode::getCopy()
{
	return std::make_unique<ExpParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr ExpParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		std::make_unique<ExpParseNode> (
			m_Operand->getCopy()
		),
		m_Operand->getDifferential ( variable )
	);
}

std::function<double(std::unordered_map<std::string,double>&)> ExpParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::exp ( operand ( parameters ) );
	};
}

ParseNode::Ptr ExpParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string ExpParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "exp(" + m_Operand->getName() + ")";
	} else {
		return "exp" + m_Operand->getName();
	}
}

LogParseNode::LogParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

LogParseNode::LogParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr LogParseNode::getCopy()
{
	return std::make_unique<LogParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr LogParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<MultiplicationParseNode> (
			std::make_unique<LnParseNode> (
				std::make_unique<ConstantParseNode> ( 10 )
			),
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> LogParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::log10 ( operand ( parameters ) );
	};
}

ParseNode::Ptr LogParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string LogParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "log(" + m_Operand->getName() + ")";
	} else {
		return "log" + m_Operand->getName();
	}
}

SecParseNode::SecParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

SecParseNode::SecParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr SecParseNode::getCopy()
{
	return std::make_unique<SecParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr SecParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		std::make_unique<MultiplicationParseNode> (
			m_Operand->getDifferential ( variable ),
			std::make_unique<TanParseNode> (
				m_Operand->getCopy()
			)
		),
		std::make_unique<SecParseNode> (
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> SecParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return 1 / std::cos ( operand ( parameters )*ParseNode::DRGConversion );
	};
}

ParseNode::Ptr SecParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string SecParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "sec(" + m_Operand->getName() + ")";
	} else {
		return "sec" + m_Operand->getName();
	}
}

SinParseNode::SinParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

SinParseNode::SinParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr SinParseNode::getCopy()
{
	return std::make_unique<SinParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr SinParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<CosParseNode> (
			m_Operand->getCopy()
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> SinParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::sin ( ParseNode::DRGConversion * operand ( parameters ) );
	};
}

ParseNode::Ptr SinParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string SinParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "sin(" + m_Operand->getName() + ")";
	} else {
		return "sin" + m_Operand->getName();
	}
}

TanParseNode::TanParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

TanParseNode::TanParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr TanParseNode::getCopy()
{
	return std::make_unique<TanParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr TanParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<MultiplicationParseNode> (
		m_Operand->getDifferential ( variable ),
		std::make_unique<PowerParseNode> (
			std::make_unique<SecParseNode> (
				m_Operand->getCopy()
			),
			std::make_unique<ConstantParseNode> ( 2 )
		)
	);
}

std::function<double(std::unordered_map<std::string,double>&)> TanParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::tan ( operand ( parameters ) * ParseNode::DRGConversion );
	};
}

ParseNode::Ptr TanParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string TanParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "tan(" + m_Operand->getName() + ")";
	} else {
		return "tan" + m_Operand->getName();
	}
}

LnParseNode::LnParseNode ( std::queue<std::unique_ptr<ParseToken>>& tokens )
{
	if ( !tokens.empty() ) {
		auto operandToken = std::move ( tokens.front() );
		tokens.pop();
		m_Operand = operandToken->getNodePtr ( tokens );
	} else {
		m_Operand = std::unique_ptr<ParseNode>();
	}
}

LnParseNode::LnParseNode ( ParseNode::Ptr op ):
	m_Operand ( std::move ( op ) )
{
}

ParseNode::Ptr LnParseNode::getCopy()
{
	return std::make_unique<LnParseNode> ( m_Operand->getCopy() );
}

ParseNode::Ptr LnParseNode::getDifferential ( std::string variable )
{
	return std::make_unique<DivisionParseNode> (
		m_Operand->getDifferential ( variable ),
		m_Operand->getCopy()
	);
}

std::function<double(std::unordered_map<std::string,double>&)> LnParseNode::getFunction()
{
	auto operand = m_Operand->getFunction();
	return [ operand ] ( std::unordered_map<std::string,double>& parameters ) -> double {
		return std::log ( operand ( parameters ) );
	};
}

ParseNode::Ptr LnParseNode::simplify()
{
	m_Operand = m_Operand->simplify();
	std::unordered_map<std::string,double> parameters;
	if ( m_Operand->isConstant() ) {
		return std::make_unique<ConstantParseNode> (
			getFunction() ( parameters )
		);
	} else {
		return getCopy();
	}
}

std::string LnParseNode::getName() const
{
	if ( m_Operand->getPriority() < getPriority() ) {
		return "ln(" + m_Operand->getName() + ")";
	} else {
		return "ln" + m_Operand->getName();
	}
}

}
